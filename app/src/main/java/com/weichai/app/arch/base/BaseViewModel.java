package com.weichai.app.arch.base;

import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.weichai.app.App;
import com.weichai.app.arch.config.RouterParams;
import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.page.common.dialog.AlertDialog;

/**
 * @ClassName: BaseViewModel
 * @Description:
 * @Author: 祖安
 * @Date: 2021/7/28 10:41 上午
 */
public class BaseViewModel extends ViewModel {
    protected int PAGE_SIZE = 10;
    protected int PAGE_NUM = 1;
    public UnPeekLiveData<RouterParams> navigationData = new UnPeekLiveData<>();//页面跳转
    public UnPeekLiveData<Integer> navigationPopData = new UnPeekLiveData<>();//页面返回
    public UnPeekLiveData<Boolean> showLoadingData = new UnPeekLiveData<>();//加载中弹窗
    public UnPeekLiveData<AlertDialog.AlertHint> alertData = new UnPeekLiveData<>();//提示弹窗
    public UnPeekLiveData<String> toastData = new UnPeekLiveData<>();//吐司

    @Override
    protected void onCleared() {
        super.onCleared();

    }

    /**
     * 路由跳转
     *
     * @param pageId 页面Id
     */
    protected void navigate(@IdRes int pageId) {
        navigationData.postValue(new RouterParams(pageId));
    }

    /**
     * 路由跳转 带参数
     *
     * @param pageId
     * @param bundle
     */
    protected void navigate(@IdRes int pageId, Bundle bundle) {
        navigationData.postValue(new RouterParams(pageId, bundle));
    }

    /**
     * 路由跳转返回上一页
     */
    protected void popPage() {
        navigationPopData.postValue(-1);
    }

    /**
     * 返回主页
     */
    protected void popPage(boolean isBackHome) {
        navigationPopData.postValue(1);
    }

    /**
     * 显示/隐藏 加载中弹窗
     *
     * @param isShow
     */
    public void showLoading(boolean isShow) {
        showLoadingData.postValue(isShow);
    }

    /**
     * 吐司信息
     *
     * @param msg
     */
    public void showToast(String msg) {
        toastData.postValue(msg);
    }

    /**
     * 获取 Application ViewModel
     *
     * @param clz Model 类
     * @return ViewModel
     */
    public <T extends ViewModel> T getViewModelOfApp(Class<T> clz) {
        return new ViewModelProvider(App.getApp()).get(clz);
    }

    /**
     * 提示弹窗
     * @param content 文字
     * @param onConfirmListener 回调
     */
    public void showAlert(String content, AlertDialog.OnConfirmListener onConfirmListener){
        alertData.postValue(new AlertDialog.AlertHint(content,false,onConfirmListener));
    }

}
