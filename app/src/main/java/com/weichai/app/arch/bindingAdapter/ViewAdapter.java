package com.weichai.app.arch.bindingAdapter;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.weichai.app.App;
import com.weichai.app.R;

import java.util.List;

/**
 * @ClassName: AdViewAdapter
 * @Description:
 * @Author: asanant
 * @Date: 2021/11/25 9:51 上午
 */
public class ViewAdapter {

    //密码  显示/隐藏
    @BindingAdapter("showEtPwd")
    public static void showEtPwd(EditText editText, boolean isShow) {
        if (editText != null) {
            int cursorPosition = editText.length();
            if (isShow) {
                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            } else {
                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
            editText.setSelection(cursorPosition);
        }
    }


    //加载显示头像
    @BindingAdapter("showAvatar")
    public static void showAvatar(ImageView imageView, String url) {
        Glide.with(App.getApp())
                .load(url)
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .error(R.mipmap.ic_defualt_avatar).into(imageView);
    }


    /**
     * @param bgColor 颜色
     */
    @BindingAdapter(value = {"radius_size", "radius_color"}, requireAll = false)
    public static void bgRadiusColor(View view, float radius, String bgColor) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(radius);
        gradientDrawable.setColor(Color.parseColor(bgColor));
        view.setBackground(gradientDrawable);
    }


    /**
     * 绑定Fragment 到ViewPager2
     *
     * @param viewPager2
     * @param fragments
     */
    @BindingAdapter("binding_adapter")
    public static void viewPager2Adapter(ViewPager2 viewPager2, List<Fragment> fragments) {
        viewPager2.setAdapter(new FragmentStateAdapter((FragmentActivity) viewPager2.getContext()) {

            @Override
            public int getItemCount() {
                return fragments.size();
            }


            @NonNull
            @Override
            public Fragment createFragment(int position) {
                return fragments.get(position);
            }
        });
    }

    @BindingAdapter("bd_visible")
    public static void viewVisible(View view, boolean visible) {
        if (visible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }

    }


}
