package com.weichai.app.arch.http;

import java.io.IOException;

/**
 * @ClassName: ApiException
 * @Description: 请求异常处理
 * @Author: asanant
 * @Date: 2021/11/15 11:25 下午
 */
public class ApiException extends IOException {
    public String code;///异常状态码
    public String msg;//异常信息

    public ApiException( String code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}
