/**
 * @Interface: ApiServer
 * @Description:
 * @Author: asanant
 * @Date: 2021/7/19 11:09 上午
 */
package com.weichai.app.arch.http;

import com.weichai.app.bean.AddEngineReq;
import com.weichai.app.bean.AdvEntity;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.ChangePhoneReq;
import com.weichai.app.bean.DictRes;
import com.weichai.app.bean.EntranceEntity;
import com.weichai.app.bean.FaultCodeEntity;
import com.weichai.app.bean.InfoDetailsEntity;
import com.weichai.app.bean.MaintenanceOrderEntity;
import com.weichai.app.bean.NewsEntity;
import com.weichai.app.bean.ProtocolMsg;
import com.weichai.app.bean.ScanEntity;
import com.weichai.app.bean.UserInfo;
import com.weichai.app.bean.CityEntity;
import com.weichai.app.bean.InformationEntity;
import com.weichai.app.bean.SearchEntity;
import com.weichai.app.bean.ServiceStationEntity;

import java.util.List;
import java.util.Map;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiServer {

    //账号密码登录
    @POST("capp-user/capp/v1/user/loginByUsernameAndPassword")
    Observable<BaseResult<UserInfo>> loginByPwd(@Body Map<String, String> params);

    //账号密码登录
    @POST("capp-user/capp/v1/user/noLongUsePhoneLog")
    Observable<BaseResult<UserInfo>> noLongUsePhoneLog(@Body Map<String, String> params);

    //获取用户信息
    @GET("capp-user/capp/v1/user/getUserInfo")
    Observable<BaseResult<UserInfo>> getUserInfo();

    //验证码登录
    @POST("capp-user/capp/v1/user/loginByVerificationCode")
    Observable<BaseResult<UserInfo>> loginByCode(@Body Map<String, String> params);

    //手机号验证
    @GET("capp-user/capp/v1/user/verifyPhone")
    Observable<BaseResult<Boolean>> verifyPhone(@Query("phone") String phone);

    //手机号验证
    @GET("capp-user/capp/v1/user/bindingVerifyPhone")
    Observable<BaseResult<Boolean>> bindingVerifyPhone(@Query("phone") String phone);


    //验证码登录
    @POST("capp-user/capp/v1/user/wechatLogin")
    Observable<BaseResult<UserInfo>> wechatLogin(@Query("code") String code);

    //验证码登录
    @POST("capp-user/capp/v1/user/bindingPhone")
    Observable<BaseResult<UserInfo>> bindingPhone(@Body Map<String, String> params);

    //发送验证码
    @GET("capp-user/capp/v1/user/sendVerify")
    Observable<BaseResult<String>> sendVerify(@Query("phone") String phone, @Query("type") String type);

    //注册用户
    @POST("capp-user/capp/v1/user/register")
    Observable<BaseResult<UserInfo>> userRegister(@Body Map<String, String> params);

    //找回密码
    @POST("capp-user/capp/v1/user/findPassword")
    Observable<BaseResult<UserInfo>> findPassword(@Body Map<String, String> params);

    // 资讯列表
    @POST("capp-news/capp/v1/information/page")
    Observable<BaseResult<InformationEntity>> informationPage(@Body Map<String, Object> params);

    // 资讯点赞
    @POST("capp-news/capp/v1/likesRecord/add")
    Observable<BaseResult<String>> likesRecordAdd(@Body Map<String, Object> params);

    // 资讯取消点赞
    @PUT("capp-news/capp/v1/likesRecord/modify")
    Observable<BaseResult<String>> likesRecordModify(@Body Map<String, Object> params);

    // 资讯详情
    @GET("capp-news/capp/v1/information/queryByNewsCode")
    Observable<BaseResult<InfoDetailsEntity>> queryByNewsCode(@Query("newsCode") String newsCode);

    // 分享资讯
    @GET("capp-news/capp/v1/information/shareByNewsCode")
    Observable<BaseResult<Boolean>> shareByNewsCode(@Query("newsCode") String newsCode);

    // 搜索
    @GET("capp-news/capp/v1/query/queryByApplyAndInformstion")
    Observable<BaseResult<SearchEntity>> query(@Query("applyAndInformstion") String applyAndInformstion);

    // 城市列表
    @POST("capp-news/capp/v1/area/queryMarket")
    Observable<BaseResult<List<CityEntity>>> queryMarket(@Body Map<String, Object> params);

    // 轮播图
    @POST("capp-news/capp/v1/carouselFigure/carousel")
    Observable<BaseResult<List<AdvEntity>>> carousel(@Body Map<String, Object> params);

    // 快捷功能入口
    @POST("capp-news/capp/v1/apply/queryAll")
    Observable<BaseResult<List<EntranceEntity>>> apply(@Body Map<String, Object> params);

    // 获取客服电话
    @POST("capp-news/capp/v1/phone/phoneNumber")
    Observable<BaseResult<String>> getPhoneNumber(@Body Map<String, Object> params);

    // 消息
    @POST("capp-news/capp/v1/message/queryEffective")
    Observable<BaseResult<List<NewsEntity>>> queryEffective(@Body Map<String, Object> params);

    // 扫描发动机二维码查询
    @POST("capp-vehicle/capp/v1/engins/motorByState")
    Observable<BaseResult<ScanEntity>> motorByState(@Body Map<String, Object> params);

    // 热门搜索
    @POST("capp-news/capp/v1/query/queryByHot")
    Observable<BaseResult<List<String>>> queryByHot(@Body Map<String, Object> params);

    // 故障码查询
    @POST("capp-vehicle/capp/v1/hitch/queryByFault")
    Observable<BaseResult<List<FaultCodeEntity>>> queryByFault(@Body Map<String, Object> params);

    // 附近服务站查询
    @POST("capp-vehicle/capp/v1/nearBySite/queryBySite")
    Observable<BaseResult<List<ServiceStationEntity>>> queryBySite(@Body Map<String, Object> params);

    // 首页-维修跟踪
    @POST("capp-vehicle/capp/v1/maintenanceOrder/queryOrder")
    Observable<BaseResult<List<MaintenanceOrderEntity>>> queryOrder(@Body Map<String, Object> params);

    // 首页-维修跟踪
    @GET("capp-news/capp/v1/protocol/queryByType")
    Observable<BaseResult<ProtocolMsg>> getProtocol(@Query("type") String type);

    // 我的-修改手机号
    @POST("capp-user/capp/v1/user/changePhone")
    Observable<BaseResult<String>> changePhone(@Body ChangePhoneReq changePhoneReq);

    //查询通用字典[0001-所属行业,0002-车辆用途，0003-故障描述 , 1000-整车品牌]
    @GET("capp-vehicle/common/v1/dict/{dictCode}")
    Observable<BaseResult<List<DictRes>>> getDictData(@Path("dictCode") String dictCode);

    //新增发动机管理表
    @POST("capp-vehicle/capp/v1/engins/add")
    Observable<BaseResult<String>> addEngine(@Body AddEngineReq addEngineReq);

}
