package com.weichai.app.arch.utils;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import com.weichai.app.arch.config.CacheConstant;

import dev.utils.app.AppUtils;
import dev.utils.app.toast.ToastUtils;

public class StringUtil {

    public static String getUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            if (url.startsWith("http")) {
                return url;
            } else {
                return "http://192.168.34.21:9001/" + url;
            }
        }
        return url;
    }

    /**
     * 拨打电话（跳转到拨号界面，户手动点击拨打）
     *
     * @param phoneNum 电话号码
     */
    public static void callPhone(String phoneNum) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        Uri uri = Uri.parse("tel:" + phoneNum);
        intent.setData(uri);
        AppUtils.startActivity(intent);
    }

    public static boolean isLogin() {
        String userToken = MMKVUtil.getString(CacheConstant.KEY_USER_TOKEN, "");
        if (TextUtils.isEmpty(userToken)) {
            ToastUtils.showToast("请先登录！", Toast.LENGTH_SHORT);
            return false;
        }
        return true;
    }
}
