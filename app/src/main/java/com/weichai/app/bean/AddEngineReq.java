package com.weichai.app.bean;

/**
 * create by bill on 11.12.21
 */
public class AddEngineReq {


    /**
     * activation
     */
    private String activation;
    /**
     * bindingType
     */
    private int bindingType;
    /**
     * brandCode
     */
    private String brandCode;
    /**
     * brandName
     */
    private String brandName;
    /**
     * bugTime
     */
    private String bugTime;
    /**
     * carNum
     */
    private String carNum;
    /**
     * carPurpose
     */
    private int carPurpose;
    /**
     * carTime
     */
    private String carTime;
    /**
     * createPerson
     */
    private String createPerson;
    /**
     * createTime
     */
    private String createTime;
    /**
     * engineCode
     */
    private String engineCode;
    /**
     * enginePhone
     */
    private String enginePhone;
    /**
     * extFields
     */
    private ExtFieldsDTO extFields;
    /**
     * id
     */
    private int id;
    /**
     * industryType
     */
    private int industryType;
    /**
     * instanceId
     */
    private int instanceId;
    /**
     * isFirst
     */
    private int isFirst;
    /**
     * remark
     */
    private String remark;
    /**
     * serialNumber
     */
    private String serialNumber;
    /**
     * tenantId
     */
    private int tenantId;
    /**
     * updatePerson
     */
    private String updatePerson;
    /**
     * updateTime
     */
    private String updateTime;
    /**
     * userCode
     */
    private String userCode;

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public int getBindingType() {
        return bindingType;
    }

    public void setBindingType(int bindingType) {
        this.bindingType = bindingType;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBugTime() {
        return bugTime;
    }

    public void setBugTime(String bugTime) {
        this.bugTime = bugTime;
    }

    public String getCarNum() {
        return carNum;
    }

    public void setCarNum(String carNum) {
        this.carNum = carNum;
    }

    public int getCarPurpose() {
        return carPurpose;
    }

    public void setCarPurpose(int carPurpose) {
        this.carPurpose = carPurpose;
    }

    public String getCarTime() {
        return carTime;
    }

    public void setCarTime(String carTime) {
        this.carTime = carTime;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getEngineCode() {
        return engineCode;
    }

    public void setEngineCode(String engineCode) {
        this.engineCode = engineCode;
    }

    public String getEnginePhone() {
        return enginePhone;
    }

    public void setEnginePhone(String enginePhone) {
        this.enginePhone = enginePhone;
    }

    public ExtFieldsDTO getExtFields() {
        return extFields;
    }

    public void setExtFields(ExtFieldsDTO extFields) {
        this.extFields = extFields;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIndustryType() {
        return industryType;
    }

    public void setIndustryType(int industryType) {
        this.industryType = industryType;
    }

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    public int getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(int isFirst) {
        this.isFirst = isFirst;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public static class ExtFieldsDTO {
    }
}
