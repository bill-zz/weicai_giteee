package com.weichai.app.bean;

import java.util.Objects;

public class EntranceEntity {
    private ExtFieldsBean extFields;
    private String functionCatenate;
    public String functionName;
    public String functionPicture;
    private Integer functionSort;
    private Object hot;
    private Object instanceId;
    private Object station;
    private Object tenantId;
    public int resId;

    public EntranceEntity(int resId, String functionName) {
        this.resId = resId;
        this.functionName = functionName;
    }

    public static class ExtFieldsBean {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntranceEntity that = (EntranceEntity) o;
        return Objects.equals(functionName, that.functionName);
    }
}
