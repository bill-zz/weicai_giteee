package com.weichai.app.bean;

import java.util.List;

public class HomeEntity {
    public List<AdvEntity> advEntityList;
    public List<InformationEntity.ListBean> listBeanList;
    public List<EntranceEntity> entranceList;
    public List<String> progressLiveData;
    public MaintenanceOrderEntity maintenanceOrderEntity;
}
