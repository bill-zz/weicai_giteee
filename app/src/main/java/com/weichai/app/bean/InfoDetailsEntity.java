package com.weichai.app.bean;

public class InfoDetailsEntity {
    public String content;
    private Object createPerson;
    private Object createTime;
    private String creationTime;
    private ExtFieldsBean extFields;
    private String id;
    private Object instanceId;
    public Integer likes;
    private String newsCode;
    public String picture;
    public String releaseTime;
    public String releaseTimeFormat;
    private Object remark;
    private Integer state;
    private Object tenantId;
    public String title;
    private Object updatePerson;
    private Object updateTime;
    public Integer views;
    public Integer like;

    /**
     * 分享链接
     */
    public String informationLink;

    /**
     * 是否能分享
     * 1：不能分享 0：可以分享
     */
    public int isShare;

    /**
     * 分享次数
     */
    public int shareAmount;

    public static class ExtFieldsBean {

    }
}
