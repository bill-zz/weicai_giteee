package com.weichai.app.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class InformationEntity implements Parcelable {
    private Integer endRow;
    public Boolean hasNextPage;
    private Boolean hasPreviousPage;
    private Boolean isFirstPage;
    private Boolean isLastPage;
    public List<ListBean> list;
    private Integer navigateFirstPage;
    private Integer navigateLastPage;
    private Integer navigatePages;
    private Object navigatepageNums;
    private Integer nextPage;
    private Integer pageNum;
    private Integer pageSize;
    private Integer pages;
    private Integer prePage;
    private Integer size;
    private Integer startRow;
    private Integer total;

    public static class ListBean implements Parcelable{
        private Object createPerson;
        private Object createTime;
        private Object creationTime;
        private ExtFieldsBean extFields;
        private Object id;
        private Object instanceId;
        public Integer likes;
        public String newsCode;
        public String picture;
        public String releaseTime;
        public String releaseTimeFormat;
        private Object remark;
        private Object state;
        private Object tenantId;
        public String title;
        private Object updatePerson;
        private Object updateTime;
        public Integer views;
        public Integer like;
        public Integer top;

        public ListBean() {

        }

        public static class ExtFieldsBean {

        }

        public ListBean(Parcel in) {
            if (in.readByte() == 0) {
                likes = null;
            } else {
                likes = in.readInt();
            }
            newsCode = in.readString();
            picture = in.readString();
            releaseTime = in.readString();
            releaseTimeFormat = in.readString();
            title = in.readString();
            if (in.readByte() == 0) {
                views = null;
            } else {
                views = in.readInt();
            }
            if (in.readByte() == 0) {
                like = null;
            } else {
                like = in.readInt();
            }
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (likes == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(likes);
            }
            dest.writeString(newsCode);
            dest.writeString(picture);
            dest.writeString(releaseTime);
            dest.writeString(releaseTimeFormat);
            dest.writeString(title);
            if (views == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(views);
            }
            if (like == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(like);
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<ListBean> CREATOR = new Creator<ListBean>() {
            @Override
            public ListBean createFromParcel(Parcel in) {
                return new ListBean(in);
            }

            @Override
            public ListBean[] newArray(int size) {
                return new ListBean[size];
            }
        };
    }

    protected InformationEntity(Parcel in) {
        if (in.readByte() == 0) {
            endRow = null;
        } else {
            endRow = in.readInt();
        }
        byte tmpHasNextPage = in.readByte();
        hasNextPage = tmpHasNextPage == 0 ? null : tmpHasNextPage == 1;
        byte tmpHasPreviousPage = in.readByte();
        hasPreviousPage = tmpHasPreviousPage == 0 ? null : tmpHasPreviousPage == 1;
        byte tmpIsFirstPage = in.readByte();
        isFirstPage = tmpIsFirstPage == 0 ? null : tmpIsFirstPage == 1;
        byte tmpIsLastPage = in.readByte();
        isLastPage = tmpIsLastPage == 0 ? null : tmpIsLastPage == 1;
        if (in.readByte() == 0) {
            navigateFirstPage = null;
        } else {
            navigateFirstPage = in.readInt();
        }
        if (in.readByte() == 0) {
            navigateLastPage = null;
        } else {
            navigateLastPage = in.readInt();
        }
        if (in.readByte() == 0) {
            navigatePages = null;
        } else {
            navigatePages = in.readInt();
        }
        if (in.readByte() == 0) {
            nextPage = null;
        } else {
            nextPage = in.readInt();
        }
        if (in.readByte() == 0) {
            pageNum = null;
        } else {
            pageNum = in.readInt();
        }
        if (in.readByte() == 0) {
            pageSize = null;
        } else {
            pageSize = in.readInt();
        }
        if (in.readByte() == 0) {
            pages = null;
        } else {
            pages = in.readInt();
        }
        if (in.readByte() == 0) {
            prePage = null;
        } else {
            prePage = in.readInt();
        }
        if (in.readByte() == 0) {
            size = null;
        } else {
            size = in.readInt();
        }
        if (in.readByte() == 0) {
            startRow = null;
        } else {
            startRow = in.readInt();
        }
        if (in.readByte() == 0) {
            total = null;
        } else {
            total = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (endRow == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(endRow);
        }
        dest.writeByte((byte) (hasNextPage == null ? 0 : hasNextPage ? 1 : 2));
        dest.writeByte((byte) (hasPreviousPage == null ? 0 : hasPreviousPage ? 1 : 2));
        dest.writeByte((byte) (isFirstPage == null ? 0 : isFirstPage ? 1 : 2));
        dest.writeByte((byte) (isLastPage == null ? 0 : isLastPage ? 1 : 2));
        if (navigateFirstPage == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(navigateFirstPage);
        }
        if (navigateLastPage == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(navigateLastPage);
        }
        if (navigatePages == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(navigatePages);
        }
        if (nextPage == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(nextPage);
        }
        if (pageNum == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pageNum);
        }
        if (pageSize == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pageSize);
        }
        if (pages == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pages);
        }
        if (prePage == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(prePage);
        }
        if (size == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(size);
        }
        if (startRow == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(startRow);
        }
        if (total == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(total);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InformationEntity> CREATOR = new Creator<InformationEntity>() {
        @Override
        public InformationEntity createFromParcel(Parcel in) {
            return new InformationEntity(in);
        }

        @Override
        public InformationEntity[] newArray(int size) {
            return new InformationEntity[size];
        }
    };
}
