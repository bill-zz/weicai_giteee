package com.weichai.app.bean;

public class MaintenanceOrderEntity {
    private Object createPerson;
    private Object createTime;
    private String engineCode;
    private ExtFieldsBean extFields;
    private Object id;
    private Object instanceId;
    public Integer maintenanceState;
    private String maintenanceTime;
    private Integer maintenanceType;
    private Object remark;
    private Object serviceCode;
    public String serviceStation;
    private Object tenantId;
    private Object updatePerson;
    private Object updateTime;
    private Object userCode;
    public String workNumber;

    public static class ExtFieldsBean {

    }
}
