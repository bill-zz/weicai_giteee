package com.weichai.app.bean;

/**
 * create by bill on 6.12.21
 */
public class MineCouponsEntity {

    private String engineNumber;

    public MineCouponsEntity(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }
}

