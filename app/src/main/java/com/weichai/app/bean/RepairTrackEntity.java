package com.weichai.app.bean;

/**
 * create by bill on 4.12.21
 */
public class RepairTrackEntity {

    private String no;
    private String station;
    private String time;

    public RepairTrackEntity(String no, String station, String time) {
        this.no = no;
        this.station = station;
        this.time = time;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
