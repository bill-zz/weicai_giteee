package com.weichai.app.bean;

import java.util.List;

public class SearchEntity {
    public int type;
    public List<SearchEntranceEntity> applyDtos;
    public List<SearchInfoEntity> informationDtos;

    public SearchEntity(int type, List<SearchEntranceEntity> applyDtos, List<SearchInfoEntity> informationDtos) {
        this.type = type;
        this.applyDtos = applyDtos;
        this.informationDtos = informationDtos;
    }
}
