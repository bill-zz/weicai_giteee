package com.weichai.app.bean;

import java.util.List;

public class SpeechEntity {
    private List<String> results_recognition;
    private String result_type;
    public String best_result;
    private OriginResultBean origin_result;
    public int error;

    public static class OriginResultBean {
        private Long corpus_no;
        private Integer err_no;
        private ResultBean result;
        private String sn;

        public static class ResultBean {
            private List<String> word;
        }
    }
}
