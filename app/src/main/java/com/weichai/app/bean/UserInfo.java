package com.weichai.app.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @ClassName: UserInfo
 * @Description:
 * @Author: asanant
 * @Date: 2021/11/15 9:07 下午
 */
public class UserInfo implements Parcelable {
    public String nickName;
    public String userName;
    public String userCode;
    public String header;
    public String token;
    public String openId;
    public String unionId;
    public int phoneBindingStatus;//是否绑定手机


    protected UserInfo(Parcel in) {
        nickName = in.readString();
        userName = in.readString();
        userCode = in.readString();
        header = in.readString();
        token = in.readString();
        openId = in.readString();
        unionId = in.readString();
        phoneBindingStatus = in.readInt();
    }

    public static final Creator<UserInfo> CREATOR = new Creator<UserInfo>() {
        @Override
        public UserInfo createFromParcel(Parcel in) {
            return new UserInfo(in);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nickName);
        parcel.writeString(userName);
        parcel.writeString(userCode);
        parcel.writeString(header);
        parcel.writeString(token);
        parcel.writeString(openId);
        parcel.writeString(unionId);
        parcel.writeInt(phoneBindingStatus);
    }
}
