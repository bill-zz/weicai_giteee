package com.weichai.app.page.common;

import androidx.databinding.ObservableField;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.MaintenanceOrderEntity;
import com.weichai.app.bean.ProtocolMsg;

import java.util.List;

/**
 * @ClassName: PrivacyVM
 * @Description:
 * @Author: asanant
 * @Date: 2021/11/26 7:12 下午
 */
public class PrivacyVM extends BaseViewModel {
    public ObservableField<String> content = new ObservableField<>();


    public void getProtocol(String type) {
        ApiManager
                .getInstance()
                .getProtocol(type)
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<ProtocolMsg>(true, true, this) {
                    @Override
                    public void onSuccess(BaseResult<ProtocolMsg> result) {
                        content.set(result.data.message);
                    }
                });
    }
}
