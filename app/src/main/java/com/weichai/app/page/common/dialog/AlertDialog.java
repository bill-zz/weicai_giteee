package com.weichai.app.page.common.dialog;


import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;


import com.weichai.app.R;

import razerdp.basepopup.BasePopupWindow;

/**
 * @ClassName: AlertDialog
 * @Description: 公共提醒弹窗
 * @Author: 祖安
 * @Date: 2021/11/29 8:55 下午
 */
public class AlertDialog extends BasePopupWindow {
    private TextView mTvContent;
    private TextView mTvConfirm;
    private AlertHint alertHint;

    public AlertDialog(Context context) {
        super(context);
        setContentView(R.layout.dialog_alert);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        mTvContent = findViewById(R.id.tv_content);
        mTvConfirm = findViewById(R.id.tv_confirm);

    }

    public void showAlert(AlertHint alertHint) {
        this.alertHint = alertHint;
        if (!alertHint.canDismiss) {
            setOutSideDismiss(false);
            setBackPressEnable(false);
        }
        setPopupGravity(Gravity.CENTER);
        showPopupWindow();
    }

    @Override
    public void onShowing() {
        super.onShowing();
        mTvContent.setText(alertHint.content);
        mTvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (alertHint.confirmListener != null) {
                    alertHint.confirmListener.onConfirm();
                }
            }
        });
    }

    public static class AlertHint {
        public String content;
        public boolean canDismiss = true;
        public OnConfirmListener confirmListener;

        public AlertHint(String content, OnConfirmListener confirmListener) {
            this.content = content;
            this.confirmListener = confirmListener;
        }

        public AlertHint(String content, boolean canDismiss, OnConfirmListener confirmListener) {
            this.content = content;
            this.canDismiss = canDismiss;
            this.confirmListener = confirmListener;
        }
    }

    public interface OnConfirmListener {
        void onConfirm();

        void onCancel();
    }
}


