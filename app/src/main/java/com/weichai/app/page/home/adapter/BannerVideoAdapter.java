package com.weichai.app.page.home.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.weichai.app.R;
import com.weichai.app.arch.utils.StringUtil;
import com.weichai.app.bean.AdvEntity;
import com.youth.banner.adapter.BannerAdapter;

import java.util.List;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;

public class BannerVideoAdapter extends BannerAdapter<AdvEntity, BannerVideoAdapter.BannerVideoHolder> {
    private OnBannerListener onBannerListener;
    private OnClickListener onClickListener;

    public BannerVideoAdapter(List<AdvEntity> advEntityList) {
        super(advEntityList);
    }

    @Override
    public BannerVideoHolder onCreateHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_banner_video,
                parent, false);
        return new BannerVideoHolder(itemView);
    }

    @Override
    public void onBindView(BannerVideoHolder holder, AdvEntity advEntity, int position, int size) {
        holder.ivImage.setVisibility(View.VISIBLE);
        holder.flVideo.setVisibility(View.GONE);
        holder.ivPlay.setVisibility(View.GONE);
        Glide.with(holder.itemView)
                .load(StringUtil.getUrl(advEntity.picture))
                .error(R.mipmap.ic_app_logo)
                .centerCrop()
                .into(holder.ivImage);
        if (advEntity.type == 1) {
            // 视频
            holder.ivPlay.setVisibility(View.VISIBLE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != onClickListener) {
                        onClickListener.onClick(v);
                        holder.ivImage.setVisibility(View.GONE);
                        holder.flVideo.setVisibility(View.VISIBLE);
                        holder.ivPlay.setVisibility(View.GONE);
                        holder.jzvdStd.fullscreenButton.setVisibility(View.GONE);
                        holder.jzvdStd.setUp(advEntity.picture, "", Jzvd.SCREEN_NORMAL);
                        holder.jzvdStd.startVideo();
                    }
                }
            });
        } else {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != onBannerListener) {
                        holder.itemView.setOnClickListener(view -> onBannerListener.OnBannerClick(advEntity, position));
                    }
                }
            });
        }
        holder.jzvdStd.startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onClickListener) {
                    onClickListener.onClick(v);
                    holder.jzvdStd.clickStart();
                }
            }
        });

        holder.jzvdStd.textureViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onClickListener) {
                    onClickListener.onClick(v);
                }
            }
        });
    }

    public static class BannerVideoHolder extends RecyclerView.ViewHolder {
        ImageView ivImage;
        FrameLayout flVideo;
        JzvdStd jzvdStd;
        ImageView ivPlay;

        public BannerVideoHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.iv_image);
            flVideo = itemView.findViewById(R.id.fl_video);
            jzvdStd = itemView.findViewById(R.id.jz_video);
            ivPlay = itemView.findViewById(R.id.iv_play);
        }
    }

    public interface OnBannerListener {

        /**
         * 点击事件
         *
         * @param data     数据实体
         * @param position 当前位置
         */
        void OnBannerClick(AdvEntity data, int position);
    }

    public void setOnBannerListener(OnBannerListener onBannerListener) {
        this.onBannerListener = onBannerListener;
    }

    public interface OnClickListener {

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        void onClick(View v);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void releaseAllVideos() {
        Jzvd.releaseAllVideos();
    }
}
