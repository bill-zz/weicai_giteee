package com.weichai.app.page.home.adapter;

import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.DraggableModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.EntranceEntity;
import com.weichai.app.databinding.ItemEntranceBinding;

public class EntranceAdapter extends BaseQuickAdapter<EntranceEntity, BaseViewHolder> implements DraggableModule {
    public boolean isEdit = false;
    public boolean isService = false;

    public EntranceAdapter() {
        super(R.layout.item_entrance);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, EntranceEntity entranceEntity) {
        ItemEntranceBinding binding = baseViewHolder.getBinding();
        if (null != binding) {
            if (TextUtils.isEmpty(entranceEntity.functionPicture)) {
                Glide.with(getContext()).load(entranceEntity.resId).into(binding.ivIcon);
            } else {
                Glide.with(getContext()).load(entranceEntity.functionPicture).into(binding.ivIcon);
            }
            binding.tvName.setText(entranceEntity.functionName);
            if (isEdit) {
                binding.ivStatus.setVisibility(View.VISIBLE);
                if (isService) {
                    binding.ivStatus.setImageResource(R.mipmap.ic_add_app);
                } else {
                    binding.ivStatus.setImageResource(R.mipmap.ic_delete_app);
                }
            } else {
                binding.ivStatus.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public void setService(boolean isService) {
        this.isService = isService;
    }
}
