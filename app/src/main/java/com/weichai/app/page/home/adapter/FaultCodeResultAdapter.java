package com.weichai.app.page.home.adapter;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.FaultCodeEntity;
import com.weichai.app.databinding.ItemFaultCodeResultBinding;

public class FaultCodeResultAdapter extends BaseQuickAdapter<FaultCodeEntity, BaseViewHolder> {
    public FaultCodeResultAdapter() {
        super(R.layout.item_fault_code_result);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, FaultCodeEntity faultCodeEntity) {
        ItemFaultCodeResultBinding binding = baseViewHolder.getBinding();
        if (null != binding && null != faultCodeEntity) {
            binding.tvHitchDescribe.setText(faultCodeEntity.hitchDescribe);
            binding.tvEcuVersion.setText(faultCodeEntity.ecuVersion);
        }
    }
}
