package com.weichai.app.page.home.adapter;

import android.app.Activity;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager2.widget.ViewPager2;

import com.chad.library.adapter.base.BaseDelegateMultiAdapter;
import com.chad.library.adapter.base.delegate.BaseMultiTypeDelegate;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.AdvEntity;
import com.weichai.app.bean.EntranceEntity;
import com.weichai.app.bean.HomeEntity;
import com.weichai.app.bean.InformationEntity;
import com.weichai.app.databinding.ItemHomeBannerBinding;
import com.weichai.app.databinding.ItemHomeEntranceBinding;
import com.weichai.app.databinding.ItemHomeInfoBinding;
import com.weichai.app.databinding.ItemHomeMaintenanceBinding;
import com.weichai.app.databinding.ItemHomeNewsBinding;
import com.youth.banner.listener.OnPageChangeListener;

import java.util.List;

import dev.utils.app.ResourceUtils;

public class HomeAdapter extends BaseDelegateMultiAdapter<HomeEntity, BaseViewHolder> {
    public static final int BANNER = 0;
    public static final int ENTRANCE = 1;
    public static final int NEWS = 2;
    public static final int MAINTENANCE = 3;
    public static final int INFO = 4;
    private final Activity context;
    private OnEntranceItemClickListener onEntranceItemClickListener;
    private OnBannerClickListener onBannerClickListener;
    private OnInfoItemClickListener onInfoItemClickListener;
    private OnInfoItemChildClickListener onInfoItemChildClickListener;
    private OnInfoMoreClickListener onInfoMoreClickListener;
    private OnNewsMoreClickListener onNewsMoreClickListener;

    public HomeAdapter(Activity context) {
        this.context = context;
        setMultiTypeDelegate(new BaseMultiTypeDelegate<HomeEntity>() {
            @Override
            public int getItemType(@NonNull List<? extends HomeEntity> list, int position) {
                switch (position) {
                    case 0:
                        return BANNER;
                    case 1:
                        return ENTRANCE;
                    case 2:
                        return NEWS;
                    case 3:
                        return MAINTENANCE;
                    case 4:
                        return INFO;
                }
                return -1;
            }
        });
        getMultiTypeDelegate().addItemType(BANNER, R.layout.item_home_banner)
                .addItemType(ENTRANCE, R.layout.item_home_entrance)
                .addItemType(NEWS, R.layout.item_home_news)
                .addItemType(MAINTENANCE, R.layout.item_home_maintenance)
                .addItemType(INFO, R.layout.item_home_info);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, HomeEntity searchEntity) {
        if (null == searchEntity) {
            return;
        }
        switch (baseViewHolder.getItemViewType()) {
            case BANNER:
                ItemHomeBannerBinding homeBannerBinding = baseViewHolder.getBinding();
                if (null != homeBannerBinding) {
                    BannerVideoAdapter bannerVideoAdapter = new BannerVideoAdapter(searchEntity.advEntityList);
                    homeBannerBinding.banner.setAdapter(bannerVideoAdapter)
                            .addBannerLifecycleObserver((LifecycleOwner) context)//添加生命周期观察者
                            .setIndicator(homeBannerBinding.indicator, false);

                    bannerVideoAdapter.setOnBannerListener(new BannerVideoAdapter.OnBannerListener() {
                        @Override
                        public void OnBannerClick(AdvEntity data, int position) {
                            if (null != onBannerClickListener) {
                                onBannerClickListener.onBannerClick(data);
                            }
                        }
                    });

                    bannerVideoAdapter.setOnClickListener(new BannerVideoAdapter.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            homeBannerBinding.banner.isAutoLoop(false);
                        }
                    });

                    homeBannerBinding.banner.addOnPageChangeListener(new OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {
                            if (state == ViewPager2.SCROLL_STATE_DRAGGING) {
                                homeBannerBinding.banner.isAutoLoop(true);
                                bannerVideoAdapter.releaseAllVideos();
                            }
                        }
                    });
                }
                break;

            case ENTRANCE:
                ItemHomeEntranceBinding homeEntranceBinding = baseViewHolder.getBinding();
                if (null != homeEntranceBinding) {
                    // 快捷功能入口
                    EntranceAdapter entranceAdapter = new EntranceAdapter();
                    homeEntranceBinding.rvEntrance.setLayoutManager(new GridLayoutManager(context, 4));
                    homeEntranceBinding.rvEntrance.setAdapter(entranceAdapter);
                    entranceAdapter.setList(searchEntity.entranceList);
                    entranceAdapter.setOnItemClickListener((adapter, view, position) -> {
                        EntranceEntity entranceEntity = (EntranceEntity) adapter.getData().get(position);
                        if (null != onEntranceItemClickListener) {
                            onEntranceItemClickListener.onEntranceItemClick(entranceEntity);
                        }
                    });
                }
                break;

            case NEWS:
                ItemHomeNewsBinding homeNewsBinding = baseViewHolder.getBinding();
                if (null != homeNewsBinding) {
                    homeNewsBinding.llNews.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (null != onNewsMoreClickListener) {
                                onNewsMoreClickListener.onNewsMoreClick();
                            }
                        }
                    });
                }
                break;

            case MAINTENANCE:
                ItemHomeMaintenanceBinding homeMaintenanceBinding = baseViewHolder.getBinding();
                if (null != homeMaintenanceBinding) {
                    // 维修跟踪
                    MaintenanceProgressAdapter progressAdapter = new MaintenanceProgressAdapter();
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                    linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    homeMaintenanceBinding.rvProgress.setLayoutManager(linearLayoutManager);
                    if (homeMaintenanceBinding.rvProgress.getItemDecorationCount() == 0) {
                        homeMaintenanceBinding.rvProgress.addItemDecoration(new ProgressItemDecoration());
                    }
                    homeMaintenanceBinding.rvProgress.setAdapter(progressAdapter);
                    progressAdapter.setList(searchEntity.progressLiveData);
                }
                break;

            case INFO:
                ItemHomeInfoBinding homeInfoBinding = baseViewHolder.getBinding();
                if (null != homeInfoBinding) {
                    homeInfoBinding.llMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (null != onInfoMoreClickListener) {
                                onInfoMoreClickListener.onInfoMoreClick();
                            }
                        }
                    });
                    // 资讯宣传
                    InfoListAdapter infoListAdapter = new InfoListAdapter();
                    homeInfoBinding.rvInfo.setLayoutManager(new LinearLayoutManager(context));
                    int i = homeInfoBinding.rvInfo.getItemDecorationCount();
                    if (homeInfoBinding.rvInfo.getItemDecorationCount() == 0) {
                        homeInfoBinding.rvInfo.addItemDecoration(new InfoListItemDecoration(0,
                                ResourceUtils.getDimensionInt(R.dimen.dp_16), 0, ResourceUtils.getDimensionInt(R.dimen.dp_24)));
                    }
                    homeInfoBinding.rvInfo.setAdapter(infoListAdapter);
                    infoListAdapter.setList(searchEntity.listBeanList);
                    infoListAdapter.setOnItemClickListener((adapter, view, position) -> {
                        InformationEntity.ListBean listBean = (InformationEntity.ListBean) adapter.getData().get(position);
                        if (null != onInfoItemClickListener) {
                            onInfoItemClickListener.onInfoItemClick(listBean);
                        }
                    });

                    infoListAdapter.setOnItemChildClickListener((adapter, view, position) -> {
                        InformationEntity.ListBean listBean = (InformationEntity.ListBean) adapter.getData().get(position);
                        if (null != onInfoItemChildClickListener) {
                            onInfoItemChildClickListener.onInfoItemChildClick(listBean, position);
                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    public interface OnBannerClickListener {
        void onBannerClick(AdvEntity advEntity);
    }

    public void setOnBannerListener(OnBannerClickListener onBannerClickListener) {
        this.onBannerClickListener = onBannerClickListener;
    }

    public interface OnEntranceItemClickListener {
        void onEntranceItemClick(EntranceEntity entranceEntity);
    }

    public void setOnEntranceItemClickListener(OnEntranceItemClickListener onEntranceItemClickListener) {
        this.onEntranceItemClickListener = onEntranceItemClickListener;
    }

    public interface OnInfoItemClickListener {
        void onInfoItemClick(InformationEntity.ListBean listBean);
    }

    public void setOnInfoItemClickListener(OnInfoItemClickListener onInfoItemClickListener) {
        this.onInfoItemClickListener = onInfoItemClickListener;
    }

    public interface OnInfoItemChildClickListener {
        void onInfoItemChildClick(InformationEntity.ListBean listBean, int position);
    }

    public void setOnInfoItemChildClickListener(OnInfoItemChildClickListener onInfoItemChildClickListener) {
        this.onInfoItemChildClickListener = onInfoItemChildClickListener;
    }

    public interface OnInfoMoreClickListener {
        void onInfoMoreClick();
    }

    public void setOnInfoMoreClickListener(OnInfoMoreClickListener onInfoMoreClickListener) {
        this.onInfoMoreClickListener = onInfoMoreClickListener;
    }

    public interface OnNewsMoreClickListener {
        void onNewsMoreClick();
    }

    public void setOnNewsMoreClickListener(OnNewsMoreClickListener onNewsMoreClickListener) {
        this.onNewsMoreClickListener = onNewsMoreClickListener;
    }
}
