package com.weichai.app.page.home.adapter;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.weichai.app.R;
import com.weichai.app.bean.EntranceEntity;
import com.weichai.app.bean.InformationEntity;
import com.weichai.app.bean.SearchEntity;
import com.weichai.app.bean.ServiceStationEntity;

import java.util.List;

public class HomeBindingAdapter {

    @BindingAdapter("infoListData")
    public static void setInfoListData(RecyclerView recyclerView, List<InformationEntity.ListBean> infoListData) {
        ((InfoListAdapter) recyclerView.getAdapter()).setList(infoListData);
    }

    @BindingAdapter("progressListData")
    public static void setProgressListData(RecyclerView recyclerView, List<String> progressListData) {
        ((MaintenanceProgressAdapter) recyclerView.getAdapter()).setList(progressListData);
    }

    @BindingAdapter("entranceListData")
    public static void setEntranceListData(RecyclerView recyclerView, List<EntranceEntity> entranceEntityList) {
        ((EntranceAdapter) recyclerView.getAdapter()).setList(entranceEntityList);
    }

    @BindingAdapter("searchEntityListData")
    public static void setSearchEntityListData(RecyclerView recyclerView, List<SearchEntity> searchEntityList) {
        ((SearchAdapter) recyclerView.getAdapter()).setList(searchEntityList);
    }

    @BindingAdapter("serviceStationEntityList")
    public static void setServiceStationEntityList(RecyclerView recyclerView, List<ServiceStationEntity> serviceStationEntityList) {
        ((ServiceStationAdapter) recyclerView.getAdapter()).setList(serviceStationEntityList);
    }

    @BindingAdapter("src")
    public static void setSrc(ImageView imageView, int likeStatus) {
        if (likeStatus == 1) {
            imageView.setImageResource(R.mipmap.ic_like_selected);
        } else {
            imageView.setImageResource(R.mipmap.ic_like_normal);
        }
    }
}
