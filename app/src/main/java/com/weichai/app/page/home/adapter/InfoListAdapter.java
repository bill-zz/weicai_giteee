package com.weichai.app.page.home.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.InformationEntity;
import com.weichai.app.databinding.ItemInfoBinding;

import dev.utils.app.SizeUtils;

public class InfoListAdapter extends BaseQuickAdapter<InformationEntity.ListBean, BaseViewHolder> {
    public InfoListAdapter() {
        super(R.layout.item_info);
        addChildClickViewIds(R.id.iv_like);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, InformationEntity.ListBean listBean) {
        ItemInfoBinding binding = baseViewHolder.getBinding();
        if (null != binding && null != listBean) {
            if (listBean.top == 1) {
                // 置顶
                binding.ivTop.setVisibility(View.VISIBLE);
            } else {
                binding.ivTop.setVisibility(View.GONE);
            }
            RequestOptions requestOptions = RequestOptions.bitmapTransform(
                    new GranularRoundedCorners(SizeUtils.dipConvertPx(8),
                            SizeUtils.dipConvertPx(8), 0, 0));
            Glide.with(baseViewHolder.itemView)
                    .load(listBean.picture)
                    .error(R.mipmap.ic_app_logo)
                    .centerCrop()
                    .apply(requestOptions)
                    .into(binding.ivIcon);
            binding.tvTitle.setText(listBean.title);
            binding.tvDate.setText(listBean.releaseTimeFormat);
            binding.tvLike.setText(listBean.likes + "");
            binding.tvRead.setText(listBean.views + "");
            if (listBean.like == 1) {
                binding.ivLike.setImageResource(R.mipmap.ic_like_selected);
            } else {
                binding.ivLike.setImageResource(R.mipmap.ic_like_normal);
            }
        }
    }
}
