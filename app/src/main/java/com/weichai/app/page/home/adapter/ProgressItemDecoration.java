package com.weichai.app.page.home.adapter;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.weichai.app.R;

import dev.utils.app.ResourceUtils;
import dev.utils.app.SizeUtils;

public class ProgressItemDecoration extends RecyclerView.ItemDecoration {
    private final Paint paint;

    public ProgressItemDecoration() {
        paint = new Paint();
        paint.setColor(ResourceUtils.getColor(R.color.color_FFFF4242));
        paint.setStrokeWidth(SizeUtils.dipConvertPx(1));
        paint.setAntiAlias(true);
    }

    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int childCount = parent.getChildCount();
        View view = parent.findViewById(R.id.iv_icon);
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            if (i == 0) {
                c.drawLine(view.getRight(), view.getBottom() - (float) view.getHeight() / 2,
                        child.getRight() + view.getLeft(), view.getBottom() - (float) view.getHeight() / 2, paint);
            } else if (i != childCount - 1) {
                c.drawLine(child.getLeft() + view.getRight(), view.getBottom() - (float) view.getHeight() / 2,
                        child.getRight() + view.getLeft(), view.getBottom() - (float) view.getHeight() / 2, paint);
            }
        }
    }
}
