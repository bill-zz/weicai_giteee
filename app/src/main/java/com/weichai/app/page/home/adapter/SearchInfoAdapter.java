package com.weichai.app.page.home.adapter;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseDelegateMultiAdapter;
import com.chad.library.adapter.base.delegate.BaseMultiTypeDelegate;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.arch.utils.StringUtil;
import com.weichai.app.bean.SearchInfoEntity;
import com.weichai.app.databinding.ItemSearchInfoBinding;
import com.weichai.app.databinding.ItemSearchTitleBinding;

import java.util.List;

public class SearchInfoAdapter extends BaseDelegateMultiAdapter<SearchInfoEntity, BaseViewHolder> {
    public static final int TITLE = 1;
    public static final int INFO = 2;

    public SearchInfoAdapter() {
        setMultiTypeDelegate(new BaseMultiTypeDelegate<SearchInfoEntity>() {
            @Override
            public int getItemType(@NonNull List<? extends SearchInfoEntity> list, int position) {
                if (position == 0) {
                    return TITLE;
                } else {
                    return INFO;
                }
            }
        });
        getMultiTypeDelegate().addItemType(TITLE, R.layout.item_search_title)
                .addItemType(INFO, R.layout.item_search_info);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, SearchInfoEntity searchInfoEntity) {
        if (null == searchInfoEntity) {
            return;
        }
        switch (baseViewHolder.getItemViewType()) {
            case TITLE:
                ItemSearchTitleBinding searchTitleBinding = baseViewHolder.getBinding();
                if (null != searchTitleBinding) {
                    searchTitleBinding.tvTitle.setText("资讯");
                }
                break;
            case INFO:
                ItemSearchInfoBinding searchInfoBinding = baseViewHolder.getBinding();
                if (null != searchInfoBinding) {
                    Glide.with(getContext())
                            .load(StringUtil.getUrl(searchInfoEntity.picture))
                            .centerCrop()
                            .error(R.mipmap.ic_app_logo)
                            .into(searchInfoBinding.ivIcon);
                    searchInfoBinding.tvTitle.setText(searchInfoEntity.title);
                    searchInfoBinding.tvLike.setText(searchInfoEntity.likes + "");
                    searchInfoBinding.tvRead.setText(searchInfoEntity.views + "");
                }
                break;
            default:
                break;
        }
    }
}
