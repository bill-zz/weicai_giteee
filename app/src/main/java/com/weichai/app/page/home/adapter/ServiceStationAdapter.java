package com.weichai.app.page.home.adapter;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.ServiceStationEntity;
import com.weichai.app.databinding.ItemServiceStationBinding;

public class ServiceStationAdapter extends BaseQuickAdapter<ServiceStationEntity, BaseViewHolder> {

    public ServiceStationAdapter() {
        super(R.layout.item_service_station);
        addChildClickViewIds(R.id.iv_phone);
    }

    @Override
    protected void onItemViewHolderCreated(@NonNull BaseViewHolder viewHolder, int viewType) {
        DataBindingUtil.bind(viewHolder.itemView);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ServiceStationEntity serviceStationEntity) {
        ItemServiceStationBinding binding = baseViewHolder.getBinding();
        if (null != binding) {
            binding.tvName.setText(serviceStationEntity.serviceStation);
            binding.tvAddress.setText(serviceStationEntity.detailedAddress);
        }
    }
}
