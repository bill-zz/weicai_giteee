package com.weichai.app.page.home.dialog;


import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.weichai.app.R;

import razerdp.basepopup.BasePopupWindow;

/**
 * 通用取消确认弹框
 */
public class CancelConfirmPopup extends BasePopupWindow {
    private TextView mTvContent, mTvTitle, mTvSecondaryTitle;
    private TextView mTvCancel;
    private TextView mTvConfirm;
    private AlertHint alertHint;

    public CancelConfirmPopup(Context context) {
        super(context);
        setContentView(R.layout.popup_cancel_confirm);
    }

    @Override
    public void onViewCreated(@NonNull View contentView) {
        super.onViewCreated(contentView);
        mTvContent = findViewById(R.id.tv_content);
        mTvTitle = findViewById(R.id.tv_title);
        mTvSecondaryTitle = findViewById(R.id.tv_secondary_title);
        mTvCancel = findViewById(R.id.tv_cancel);
        mTvConfirm = findViewById(R.id.tv_confirm);

    }

    public void showAlert(AlertHint alertHint) {
        this.alertHint = alertHint;
        if (!alertHint.canDismiss) {
            setOutSideDismiss(false);
            setBackPressEnable(false);
        }
        if (alertHint.showTitle) {
            mTvContent.setVisibility(View.GONE);
            mTvTitle.setVisibility(View.VISIBLE);
            mTvSecondaryTitle.setVisibility(View.VISIBLE);
        }
        setPopupGravity(Gravity.CENTER);
        showPopupWindow();
    }

    @Override
    public void onShowing() {
        super.onShowing();
        mTvContent.setText(alertHint.content);
        mTvTitle.setText(alertHint.title);
        mTvSecondaryTitle.setText(alertHint.secondaryTitle);
        mTvCancel.setText(alertHint.cancel);
        mTvConfirm.setText(alertHint.confirm);
        mTvCancel.setOnClickListener(v -> {
            dismiss();
            if (alertHint.confirmListener != null) {
                alertHint.confirmListener.onCancel();
            }
        });

        mTvConfirm.setOnClickListener(view -> {
            dismiss();
            if (alertHint.confirmListener != null) {
                alertHint.confirmListener.onConfirm();
            }
        });
    }

    public static class AlertHint {
        public String content;
        public String title;
        public String secondaryTitle;
        public String cancel;
        public String confirm;
        public boolean canDismiss = true;
        public boolean showTitle = false;
        public OnConfirmListener confirmListener;

        public AlertHint(String content, String cancel, String confirm, OnConfirmListener confirmListener) {
            this.content = content;
            this.cancel = cancel;
            this.confirm = confirm;
            this.confirmListener = confirmListener;
        }

        public AlertHint(String title, String secondaryTitle, String cancel, String confirm, boolean showTitle, OnConfirmListener confirmListener) {
            this.title = title;
            this.secondaryTitle = secondaryTitle;
            this.cancel = cancel;
            this.confirm = confirm;
            this.showTitle = showTitle;
            this.confirmListener = confirmListener;
        }

        public AlertHint(String content, String cancel, String confirm, boolean canDismiss, OnConfirmListener confirmListener) {
            this.content = content;
            this.cancel = cancel;
            this.confirm = confirm;
            this.canDismiss = canDismiss;
            this.confirmListener = confirmListener;
        }
    }

    public interface OnConfirmListener {
        void onConfirm();

        void onCancel();
    }
}


