package com.weichai.app.page.home.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.CityEntity;
import com.weichai.app.databinding.FragmentCitySelectBinding;
import com.weichai.app.page.home.viewmodel.CitySelectVM;
import com.weichai.app.page.main.MainModel;
import com.weichai.app.views.cityselect.view.CitySelectView;

public class CitySelectFragment extends BaseFragment<FragmentCitySelectBinding, CitySelectVM> {
    private String city;

    @Override
    protected void initView() {
        getViewModelOfActivity(MainModel.class).locationCity.observe(this, locationCity -> {
            mViewBinding.citySelectView.setLocationCity(mViewModel.cityEntityList.getValue(),
                    new CityEntity(locationCity));
        });
        mViewModel.cityEntityList.observe(this, cityEntityList -> {
            mViewBinding.citySelectView.bindData(cityEntityList,
                    new CityEntity(getViewModelOfActivity(MainModel.class).locationCity.getValue()));
        });

        mViewBinding.citySelectView.setOnClickListener((CitySelectView.OnClickListener) name -> {
            getViewModelOfActivity(MainModel.class).selectCity.setValue(name);
            popPage();
        });
    }

    @Override
    protected void initData() {
        mViewModel.queryMarket();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_city_select;
    }
}
