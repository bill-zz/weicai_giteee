package com.weichai.app.page.home.view;

import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.listener.OnItemDragListener;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.arch.config.RouterParams;
import com.weichai.app.arch.utils.StringUtil;
import com.weichai.app.bean.EntranceEntity;
import com.weichai.app.databinding.FragmentEditApplicationBinding;
import com.weichai.app.page.home.adapter.EntranceAdapter;
import com.weichai.app.page.home.adapter.EntranceItemDecoration;
import com.weichai.app.page.home.viewmodel.EditApplicationVM;

import java.util.Collections;

import dev.utils.app.SizeUtils;
import dev.utils.app.toast.ToastUtils;

/**
 * 应用中心
 */
public class EditApplicationFragment extends BaseFragment<FragmentEditApplicationBinding, EditApplicationVM> {
    private EntranceAdapter entranceAdapter;
    private EntranceAdapter entranceServiceAdapter;

    @Override
    protected void initView() {
        entranceAdapter = new EntranceAdapter();
        entranceAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                EntranceEntity entranceEntity = (EntranceEntity) adapter.getData().get(position);
                if (entranceAdapter.isEdit) {
                    mViewModel.entranceList.getValue().remove(entranceEntity);
                    mViewModel.entranceServiceList.getValue().add(entranceEntity);
                    entranceAdapter.setList(mViewModel.entranceList.getValue());
                    entranceServiceAdapter.setList(mViewModel.entranceServiceList.getValue());
                } else {
                    jump(entranceEntity);
                }
            }
        });
        entranceAdapter.getDraggableModule().setOnItemDragListener(new OnItemDragListener() {
            @Override
            public void onItemDragStart(RecyclerView.ViewHolder viewHolder, int pos) {

            }

            @Override
            public void onItemDragMoving(RecyclerView.ViewHolder source, int from, RecyclerView.ViewHolder target, int to) {
                if (null != mViewModel.entranceList.getValue()) {
                    Collections.swap(mViewModel.entranceList.getValue(), from, to);
                }
            }

            @Override
            public void onItemDragEnd(RecyclerView.ViewHolder viewHolder, int pos) {

            }
        });
        mViewBinding.rvHomePage.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        mViewBinding.rvHomePage.addItemDecoration(new EntranceItemDecoration(SizeUtils.dipConvertPx(15),
                SizeUtils.dipConvertPx(10), SizeUtils.dipConvertPx(15), SizeUtils.dipConvertPx(10)));
        mViewBinding.rvHomePage.setAdapter(entranceAdapter);

        entranceServiceAdapter = new EntranceAdapter();
        entranceServiceAdapter.setService(true);
        mViewBinding.rvService.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        mViewBinding.rvService.addItemDecoration(new EntranceItemDecoration(SizeUtils.dipConvertPx(15),
                SizeUtils.dipConvertPx(10), SizeUtils.dipConvertPx(15), SizeUtils.dipConvertPx(10)));
        mViewBinding.rvService.setAdapter(entranceServiceAdapter);
        entranceServiceAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                EntranceEntity entranceEntity = mViewModel.entranceServiceList.getValue().get(position);
                if (entranceServiceAdapter.isEdit) {
                    if (mViewModel.entranceList.getValue().size() > 5) {
                        ToastUtils.showToast("最多添加6个！", Toast.LENGTH_SHORT);
                        return;
                    }
                    mViewModel.entranceServiceList.getValue().remove(entranceEntity);
                    mViewModel.entranceList.getValue().add(entranceEntity);
                    entranceServiceAdapter.setList(mViewModel.entranceServiceList.getValue());
                    entranceAdapter.setList(mViewModel.entranceList.getValue());
                } else {
                    jump(entranceEntity);
                }
            }
        });

        mViewBinding.appBarView2.setRightTextOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.isEdit.setValue(true);
            }
        });

        mViewModel.isEdit.observe(this, isEdit -> {
            if (isEdit) {
                mViewBinding.appBarView2.setAppBarRightText("");
                mViewBinding.appBarView2.setAppBarTitle("编辑应用");
                entranceAdapter.setEdit(true);
                entranceServiceAdapter.setEdit(true);
                entranceAdapter.getDraggableModule().setDragEnabled(true);
            } else {
                mViewBinding.appBarView2.setAppBarRightText("管理");
                mViewBinding.appBarView2.setAppBarTitle("应用中心");
                entranceAdapter.setEdit(false);
                entranceServiceAdapter.setEdit(false);
                entranceAdapter.getDraggableModule().setDragEnabled(false);
            }
            entranceAdapter.notifyDataSetChanged();
            entranceServiceAdapter.notifyDataSetChanged();
        });
    }

    private void jump(EntranceEntity entranceEntity) {
        switch (entranceEntity.functionName) {
            case "故障码查询":
                if (StringUtil.isLogin()) {
                    navigate(new RouterParams(R.id.faultCodeFragment));
                }
                break;
            case "服务站":
                if (StringUtil.isLogin()) {
                    navigate(new RouterParams(R.id.serviceStationFragment));
                }
                break;
            case "立即维修":
                navigate(new RouterParams(R.id.ServiceRepairImmediatelyFragment));
                break;
            case "发动机":
                navigate(new RouterParams(R.id.mine_engine));
                break;
            default:
                ToastUtils.showToast("功能暂未实现！", Toast.LENGTH_SHORT);
                break;
        }
    }

    @Override
    protected void initData() {
        mViewModel.getEntranceList();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_edit_application;
    }
}
