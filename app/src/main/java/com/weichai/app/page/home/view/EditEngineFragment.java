package com.weichai.app.page.home.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentAddEngineBinding;
import com.weichai.app.databinding.FragmentEditEngineBinding;
import com.weichai.app.page.home.viewmodel.AddEngineVM;
import com.weichai.app.page.home.viewmodel.EditEngineVM;
import com.weichai.app.views.dialog.SelectItemDialog;

public class EditEngineFragment extends BaseFragment<FragmentEditEngineBinding, EditEngineVM> {

    @Override
    protected void initView() {
        mViewBinding.setFg(this);
    }

    @Override
    protected void initData() {

    }

    public void selectIndustry() {
        SelectItemDialog dialog = new SelectItemDialog(requireContext());
        dialog.setTitle("选择所属行业");
        dialog.show();
        dialog.setClickOkCallback(dictRes -> {
//            mViewModel.addEngineReq.setIndustryType(dictRes.getType());
        });
    }

    public void selectCarUse() {
        SelectItemDialog dialog = new SelectItemDialog(requireContext());
        dialog.setTitle("选择车辆用途");
        dialog.show();
        dialog.setClickOkCallback(dictRes -> {
//            mViewModel.addEngineReq.setCarPurpose(dictRes.getType());
        });
    }


    public void selectCarBrand() {
        SelectItemDialog dialog = new SelectItemDialog(requireContext());
        dialog.setTitle("选择整车品牌");
        dialog.show();
        dialog.setClickOkCallback(dictRes -> {

        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_edit_engine;
    }
}
