package com.weichai.app.page.home.view;

import android.text.Editable;
import android.text.TextWatcher;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentFaultCodeBinding;
import com.weichai.app.page.home.viewmodel.FaultCodeVM;

import java.util.ArrayList;
import java.util.List;

/**
 * 故障码查询
 */
public class FaultCodeFragment extends BaseFragment<FragmentFaultCodeBinding, FaultCodeVM> {

    @Override
    protected void initView() {
        mViewBinding.etSpn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mViewModel.spnCode.setValue(s.toString());
            }
        });
        mViewBinding.etFmi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mViewModel.fmiCode.setValue(s.toString());
            }
        });

        mViewModel.tabList.observe(this, tabList -> {
            if (null != mViewModel.tabList.getValue()) {
                List<FaultCodeResultFragment> faultCodeResultFragmentList = new ArrayList<>();
                for (String tab : mViewModel.tabList.getValue()) {
                    mViewBinding.tabLayout.addTab(mViewBinding.tabLayout.newTab());
                    faultCodeResultFragmentList.add(FaultCodeResultFragment.newInstance(mViewModel.tabMap.getValue().get(tab)));
                }

                mViewBinding.viewPager.setAdapter(new FragmentStatePagerAdapter(getParentFragmentManager()) {
                    @NonNull
                    @Override
                    public Fragment getItem(int position) {
                        return faultCodeResultFragmentList.get(position);
                    }

                    @Override
                    public int getCount() {
                        return faultCodeResultFragmentList.size();
                    }
                });
                mViewBinding.tabLayout.setupWithViewPager(mViewBinding.viewPager);
                for (int i = 0; i < mViewModel.tabList.getValue().size(); i++) {
                    mViewBinding.tabLayout.getTabAt(i).setText(mViewModel.tabList.getValue().get(i));
                }
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_fault_code;
    }
}
