package com.weichai.app.page.home.view;

import android.os.Bundle;
import android.os.Parcelable;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.FaultCodeEntity;
import com.weichai.app.databinding.FragmentFaultCodeResultBinding;
import com.weichai.app.page.home.adapter.FaultCodeResultAdapter;
import com.weichai.app.page.home.adapter.InfoListItemDecoration;
import com.weichai.app.page.home.viewmodel.FaultCodeResultVM;

import java.util.ArrayList;
import java.util.List;

import dev.utils.app.ResourceUtils;

/**
 * 故障码查询结果fragment
 */
public class FaultCodeResultFragment extends BaseFragment<FragmentFaultCodeResultBinding, FaultCodeResultVM> {

    public static FaultCodeResultFragment newInstance(List<FaultCodeEntity> faultCodeEntityList) {
        FaultCodeResultFragment faultCodeResultFragment = new FaultCodeResultFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("list", (ArrayList<? extends Parcelable>) faultCodeEntityList);
        faultCodeResultFragment.setArguments(bundle);
        return faultCodeResultFragment;
    }

    @Override
    protected void initView() {
        FaultCodeResultAdapter faultCodeResultAdapter = new FaultCodeResultAdapter();
        mViewBinding.rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        mViewBinding.rv.addItemDecoration(new InfoListItemDecoration(ResourceUtils.getDimensionInt(R.dimen.dp_16),
                ResourceUtils.getDimensionInt(R.dimen.dp_20), ResourceUtils.getDimensionInt(R.dimen.dp_16),
                ResourceUtils.getDimensionInt(R.dimen.dp_24)));
        mViewBinding.rv.setAdapter(faultCodeResultAdapter);
        if (null != getArguments()) {
            List<FaultCodeEntity> list = getArguments().getParcelableArrayList("list");
            faultCodeResultAdapter.setList(list);
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_fault_code_result;
    }
}
