package com.weichai.app.page.home.view;

import static com.weichai.app.App.APP_ID;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.View;

import androidx.lifecycle.Observer;

import com.bumptech.glide.Glide;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.InformationEntity;
import com.weichai.app.databinding.FragmentInfoDetailsBinding;
import com.weichai.app.page.home.viewmodel.InfoDetailsVM;

import java.io.ByteArrayOutputStream;

import dev.utils.app.ScreenUtils;
import dev.utils.app.SizeUtils;

public class InfoDetailsFragment extends BaseFragment<FragmentInfoDetailsBinding, InfoDetailsVM> {

    @Override
    protected void initView() {
        mViewBinding.llTitleBar.setPadding(SizeUtils.dipConvertPx(16),
                mViewBinding.llTitleBar.getTop() + ScreenUtils.getStatusBarHeight() + SizeUtils.dipConvertPx(10),
                SizeUtils.dipConvertPx(16), SizeUtils.dipConvertPx(10));

        mViewModel.picture.observe(mActivity, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (null != mActivity) {
                    Glide.with(mActivity).load(s).centerCrop().error(R.mipmap.ic_app_logo).into(mViewBinding.ivTop);
                }
            }
        });

        mViewModel.shareByNewsCodeSuccess.observe(this, shareSuccess -> {
            shareToWX();
        });

        mViewModel.isShare.observe(this, isShare -> {
            if (isShare == 0) {
                // 可以分享
                mViewBinding.clShare.setVisibility(View.VISIBLE);
            } else {
                // 不可以分享
                mViewBinding.clShare.setVisibility(View.GONE);
            }
        });
    }

    private void shareToWX() {
        //初始化一个WXWebpageObject，填写url
        WXWebpageObject webpage = new WXWebpageObject();
        if (null != mViewModel.informationLink.getValue()) {
            if (TextUtils.isEmpty(mViewModel.informationLink.getValue())) {
                webpage.webpageUrl = "https://www.weichai.com";
            } else {
                webpage.webpageUrl = mViewModel.informationLink.getValue();
            }
        } else {
            webpage.webpageUrl = "https://www.weichai.com";
        }

//用 WXWebpageObject 对象初始化一个 WXMediaMessage 对象
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = mViewModel.title.getValue();
        msg.description = mViewModel.content.getValue();

        Bitmap thumbBmp = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_app_logo);
        msg.thumbData = compressImage(thumbBmp);
//构造一个Req
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = "webpage";
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;

//调用api接口，发送数据到微信
        WXAPIFactory.createWXAPI(mActivity, APP_ID, true).sendReq(req);
    }

    /**
     * 质量压缩方法
     *
     * @param image
     * @return
     */
    public static byte[] compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 90;
        while (baos.toByteArray().length / 1024 > 32) { // 循环判断如果压缩后图片是否大于32kb,大于继续压缩
            baos.reset(); // 重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
            options -= 10;// 每次都减少10
        }
        return baos.toByteArray();
    }

    @Override
    protected void initData() {
        if (null != getArguments()) {
            mViewModel.listBean.setValue((InformationEntity.ListBean) getArguments().get("listBean"));
            mViewModel.queryByNewsCode();
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_info_details;
    }
}
