package com.weichai.app.page.home.view;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.arch.config.RouterParams;
import com.weichai.app.bean.InformationEntity;
import com.weichai.app.databinding.FragmentInformationListBinding;
import com.weichai.app.page.home.adapter.InfoListAdapter;
import com.weichai.app.page.home.adapter.InfoListItemDecoration;
import com.weichai.app.page.home.viewmodel.InformationListVM;

import dev.utils.app.ResourceUtils;

public class InformationListFragment extends BaseFragment<FragmentInformationListBinding, InformationListVM> {
    private InfoListAdapter infoListAdapter;

    @Override
    protected void initView() {
        infoListAdapter = new InfoListAdapter();
        mViewBinding.rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        mViewBinding.rv.addItemDecoration(new InfoListItemDecoration(ResourceUtils.getDimensionInt(R.dimen.dp_16),
                ResourceUtils.getDimensionInt(R.dimen.dp_16), ResourceUtils.getDimensionInt(R.dimen.dp_16),
                ResourceUtils.getDimensionInt(R.dimen.dp_24)));
        mViewBinding.rv.setAdapter(infoListAdapter);
        infoListAdapter.setOnItemClickListener((adapter, view, position) -> {
            InformationEntity.ListBean listBean = (InformationEntity.ListBean) adapter.getData().get(position);
            Bundle bundle = new Bundle();
            bundle.putParcelable("listBean", listBean);
            navigate(new RouterParams(R.id.infoDetailsFragment, bundle));
        });

        infoListAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            InformationEntity.ListBean listBean = (InformationEntity.ListBean) adapter.getData().get(position);
            if (listBean.like == 1) {
                mViewModel.likesRecordModify(listBean.newsCode, position);
            } else {
                mViewModel.likesRecordAdd(listBean.newsCode, position);
            }
        });

        infoListAdapter.setList(mViewModel.informationEntityList.getValue());
        if (null != mViewModel.loadMoreEnd.getValue() && mViewModel.loadMoreEnd.getValue()) {
            mViewBinding.refreshLayout.setEnableLoadMore(false);
            if (!infoListAdapter.hasFooterLayout()) {
                infoListAdapter.addFooterView(getFooterView(), 0);
            }
        }
        mViewModel.informationEntityList.observe(this, listBeans -> {
            infoListAdapter.setList(listBeans);
        });

        mViewModel.loadMoreEnd.observe(this, aBoolean -> {
            mViewBinding.refreshLayout.setEnableLoadMore(false);
            if (!infoListAdapter.hasFooterLayout()) {
                infoListAdapter.addFooterView(getFooterView(), 0);
            }
        });

        mViewModel.refresh.observe(this, refresh -> mViewBinding.refreshLayout.finishRefresh());
        mViewModel.loadMore.observe(this, loadMore -> mViewBinding.refreshLayout.finishLoadMore());

        mViewBinding.refreshLayout.setRefreshHeader(new ClassicsHeader(mActivity));
        mViewBinding.refreshLayout.setRefreshFooter(new ClassicsFooter(mActivity));
        mViewBinding.refreshLayout.setOnRefreshListener(refreshlayout -> {
            mViewModel.getList(true);
            refreshlayout.setEnableLoadMore(true);
            if (infoListAdapter.hasFooterLayout()) {
                infoListAdapter.removeAllFooterView();
            }
        });
        mViewBinding.refreshLayout.setOnLoadMoreListener(refreshlayout -> {
            mViewModel.getList(false);
        });
    }

    private View getFooterView() {
        return getLayoutInflater().inflate(R.layout.footer_view, mViewBinding.rv, false);
    }

    @Override
    protected void initData() {
        mViewModel.getList(false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_information_list;
    }
}
