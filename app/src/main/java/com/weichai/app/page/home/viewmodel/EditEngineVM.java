package com.weichai.app.page.home.viewmodel;

import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.http.ApiException;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.bean.AddEngineReq;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.DictRes;

import java.util.List;

import dev.utils.app.toast.ToastUtils;

public class EditEngineVM extends BaseViewModel {


   public MutableLiveData industryData = new MutableLiveData<List<DictRes>>();
   public MutableLiveData carUseData = new MutableLiveData<List<DictRes>>();
   public MutableLiveData brandData = new MutableLiveData<List<DictRes>>();
   public AddEngineReq addEngineReq = new AddEngineReq();


    public void getIndustryData() {
        //查询通用字典[0001-所属行业,0002-车辆用途，0003-故障描述 , 1000-整车品牌]
        ApiManager.getInstance().getDictData("0001").compose(new ApiTransformer<>()).subscribeWith(new ApiSubscriber<List<DictRes>>() {
            @Override
            public void onSuccess(BaseResult<List<DictRes>> data) {
                industryData.postValue(data);
            }
        });
    }

    public void getCarUseData() {
//        carUseData.
        //查询通用字典[0001-所属行业,0002-车辆用途，0003-故障描述 , 1000-整车品牌]
        ApiManager.getInstance().getDictData("0002").compose(new ApiTransformer<>()).subscribeWith(new ApiSubscriber<List<DictRes>>() {
            @Override
            public void onSuccess(BaseResult<List<DictRes>> data) {
                carUseData.postValue(data);
            }
        });
    }

    public void getCarBrandData() {
//        carUseData.
        //查询通用字典[0001-所属行业,0002-车辆用途，0003-故障描述 , 1000-整车品牌]
        ApiManager.getInstance().getDictData("1000").compose(new ApiTransformer<>()).subscribeWith(new ApiSubscriber<List<DictRes>>() {
            @Override
            public void onSuccess(BaseResult<List<DictRes>> data) {
                brandData.postValue(data);
            }
        });
    }

    public void addEngine() {


        ApiManager.getInstance().addEngine(addEngineReq).compose(new ApiTransformer<>()).subscribeWith(new ApiSubscriber<String>(true, true, this) {
            @Override
            public void onSuccess(BaseResult<String> result) {
                popPage();
            }

            @Override
            public void onFail(ApiException apiException) {
                super.onFail(apiException);
                ToastUtils.showToast(apiException.msg, Toast.LENGTH_SHORT);
            }
        });
    }

}
