package com.weichai.app.page.home.viewmodel;

import static com.weichai.app.arch.config.CacheConstant.KEY_USER_INFO;

import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.arch.utils.MMKVUtil;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.InfoDetailsEntity;
import com.weichai.app.bean.InformationEntity;
import com.weichai.app.bean.UserInfo;
import com.weichai.app.page.main.MainModel;

import java.util.HashMap;

public class InfoDetailsVM extends BaseViewModel {
    public UnPeekLiveData<String> picture = new UnPeekLiveData<>();
    public UnPeekLiveData<String> title = new UnPeekLiveData<>();
    public UnPeekLiveData<String> content = new UnPeekLiveData<>();
    public UnPeekLiveData<String> like = new UnPeekLiveData<>();
    public UnPeekLiveData<String> view = new UnPeekLiveData<>();
    public UnPeekLiveData<String> shareAmount = new UnPeekLiveData<>();
    public UnPeekLiveData<String> releaseTime = new UnPeekLiveData<>();
    public UnPeekLiveData<String> releaseTimeFormat = new UnPeekLiveData<>();
    public UnPeekLiveData<Integer> likeStatus = new UnPeekLiveData<>();
    public UnPeekLiveData<InformationEntity.ListBean> listBean = new UnPeekLiveData<>();
    public UnPeekLiveData<String> informationLink = new UnPeekLiveData<>();
    public UnPeekLiveData<Integer> isShare = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> shareByNewsCodeSuccess = new UnPeekLiveData<>();
    private Integer likes;

    /**
     * 资讯详情
     */
    public void queryByNewsCode() {
        ApiManager.getInstance()
                .queryByNewsCode(listBean.getValue().newsCode)
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<InfoDetailsEntity>(true, false, this) {
                    @Override
                    public void onSuccess(BaseResult<InfoDetailsEntity> result) {
                        if (null != result.data) {
                            picture.setValue(result.data.picture);
                            title.setValue(result.data.title);
                            content.setValue(result.data.content);
                            like.setValue(result.data.likes + "");
                            likes = result.data.likes;
                            view.setValue(result.data.views + "");
                            shareAmount.setValue(result.data.shareAmount + "");
                            informationLink.setValue(result.data.informationLink);
                            isShare.setValue(result.data.isShare);
                            releaseTime.setValue(result.data.releaseTime);
                            releaseTimeFormat.setValue(result.data.releaseTimeFormat);
                            likeStatus.setValue(result.data.like);
                        }
                    }
                });
    }

    public void share() {
        shareByNewsCode();
    }

    /**
     * 分享资讯
     */
    public void shareByNewsCode() {
        ApiManager.getInstance()
                .shareByNewsCode(listBean.getValue().newsCode)
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<Boolean>(true, false, this) {
                    @Override
                    public void onSuccess(BaseResult<Boolean> result) {
                        if (null != result.data) {
                            if (result.data) {
                                queryByNewsCode();
                            }
                            shareByNewsCodeSuccess.setValue(true);
                        }
                    }
                });
    }

    public void back() {
        popPage();
    }

    public void like() {
        if (null != likeStatus.getValue() && likeStatus.getValue() == 1) {
            likesRecordModify(listBean.getValue().newsCode);
        } else {
            likesRecordAdd(listBean.getValue().newsCode);
        }
    }

    /**
     * 点赞
     */
    public void likesRecordAdd(String newsCode) {
        UserInfo user = MMKVUtil.getParcelable(KEY_USER_INFO, UserInfo.class);
        if (user != null) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("newsCode", newsCode);
            params.put("userCode", user.userCode);

            ApiManager.getInstance()
                    .likesRecordAdd(params)
                    .compose(new ApiTransformer<>())
                    .subscribeWith(new ApiSubscriber<String>(true, false, this) {
                        @Override
                        public void onSuccess(BaseResult<String> result) {
                            likes++;
                            like.setValue(likes + "");
                            likeStatus.setValue(1);
                            listBean.getValue().likes++;
                            listBean.getValue().like = 1;
                            getViewModelOfApp(MainModel.class).clickLike.setValue(true);
                        }
                    });
        }
    }

    /**
     * 取消点赞
     */
    public void likesRecordModify(String newsCode) {
        UserInfo user = MMKVUtil.getParcelable(KEY_USER_INFO, UserInfo.class);
        if (user != null) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("newsCode", newsCode);
            params.put("userCode", user.userCode);

            ApiManager.getInstance()
                    .likesRecordModify(params)
                    .compose(new ApiTransformer<>())
                    .subscribeWith(new ApiSubscriber<String>(true, false, this) {
                        @Override
                        public void onSuccess(BaseResult<String> result) {
                            likes--;
                            like.setValue(likes + "");
                            likeStatus.setValue(0);
                            listBean.getValue().likes--;
                            listBean.getValue().like = 0;
                            getViewModelOfApp(MainModel.class).clickLike.setValue(true);
                        }
                    });
        }
    }
}
