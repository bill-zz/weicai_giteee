package com.weichai.app.page.home.viewmodel;

import static com.weichai.app.arch.config.CacheConstant.KEY_USER_INFO;

import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.http.ApiException;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.arch.utils.MMKVUtil;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.InformationEntity;
import com.weichai.app.bean.UserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InformationListVM extends BaseViewModel {
    public UnPeekLiveData<List<InformationEntity.ListBean>> informationEntityList = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> refresh = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> loadMore = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> loadMoreEnd = new UnPeekLiveData<>();

    public void getList(boolean isRefresh) {
        if (isRefresh) {
            PAGE_NUM = 1;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("pageNum", PAGE_NUM);
        params.put("pageSize", PAGE_SIZE);

        ApiManager.getInstance()
                .informationPage(params)
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<InformationEntity>(true, false, this) {
                    @Override
                    public void onSuccess(BaseResult<InformationEntity> result) {
                        PAGE_NUM++;
                        if (isRefresh) {
                            refresh.setValue(true);
                        } else {
                            loadMore.setValue(true);
                        }
                        List<InformationEntity.ListBean> listBeanList = informationEntityList.getValue();
                        InformationEntity informationEntity = result.data;
                        if (null != informationEntity) {
                            if (isRefresh) {
                                informationEntityList.setValue(informationEntity.list);
                            } else {
                                if (null == listBeanList) {
                                    listBeanList = new ArrayList<>();
                                }
                                if (null != informationEntity.list && !informationEntity.list.isEmpty()) {
                                    listBeanList.addAll(informationEntity.list);
                                    informationEntityList.setValue(listBeanList);
                                }
                            }
                            if (!informationEntity.hasNextPage) {
                                loadMoreEnd.setValue(true);
                            }
                        }
                    }

                    @Override
                    public void onFail(ApiException apiException) {
                        super.onFail(apiException);
                        if (isRefresh) {
                            refresh.setValue(true);
                        } else {
                            loadMore.setValue(true);
                        }
                    }
                });
    }

    /**
     * 点赞
     */
    public void likesRecordAdd(String newsCode, int position) {
        UserInfo user = MMKVUtil.getParcelable(KEY_USER_INFO, UserInfo.class);
        if (user != null) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("newsCode", newsCode);
            params.put("userCode", user.userCode);

            ApiManager.getInstance()
                    .likesRecordAdd(params)
                    .compose(new ApiTransformer<>())
                    .subscribeWith(new ApiSubscriber<String>(true, false, this) {
                        @Override
                        public void onSuccess(BaseResult<String> result) {
                            List<InformationEntity.ListBean> listBeanList = informationEntityList.getValue();
                            if (null != listBeanList) {
                                InformationEntity.ListBean listBean = listBeanList.get(position);
                                listBean.likes++;
                                listBean.like = 1;
                                informationEntityList.setValue(listBeanList);
                            }
                        }
                    });
        }
    }

    /**
     * 取消点赞
     */
    public void likesRecordModify(String newsCode, int position) {
        UserInfo user = MMKVUtil.getParcelable(KEY_USER_INFO, UserInfo.class);
        if (user != null) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("newsCode", newsCode);
            params.put("userCode", user.userCode);

            ApiManager.getInstance()
                    .likesRecordModify(params)
                    .compose(new ApiTransformer<>())
                    .subscribeWith(new ApiSubscriber<String>(true, false, this) {
                        @Override
                        public void onSuccess(BaseResult<String> result) {
                            List<InformationEntity.ListBean> listBeanList = informationEntityList.getValue();
                            if (null != listBeanList) {
                                InformationEntity.ListBean listBean = listBeanList.get(position);
                                listBean.likes--;
                                listBean.like = 0;
                                informationEntityList.setValue(listBeanList);
                            }
                        }
                    });
        }
    }
}
