package com.weichai.app.page.home.viewmodel;

import static com.weichai.app.arch.config.CacheConstant.KEY_USER_INFO;

import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.config.CacheConstant;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.arch.utils.MMKVUtil;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.SearchEntity;
import com.weichai.app.bean.SearchEntranceEntity;
import com.weichai.app.bean.SearchInfoEntity;
import com.weichai.app.bean.UserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dev.utils.app.toast.ToastUtils;

public class SearchVM extends BaseViewModel {
    public UnPeekLiveData<List<String>> tagHistoryLiveData = new UnPeekLiveData<>();
    public UnPeekLiveData<List<String>> tagHotLiveData = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> isShowTag = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> isShowClear = new UnPeekLiveData<>();
    public UnPeekLiveData<Boolean> isShowEmptyView = new UnPeekLiveData<>();
    public UnPeekLiveData<List<SearchEntity>> searchEntityList = new UnPeekLiveData<>();
    public UnPeekLiveData<String> searchContent = new UnPeekLiveData<>();
    private Gson gson;
    private final UserInfo userInfo = MMKVUtil.getParcelable(KEY_USER_INFO, UserInfo.class);
    private String userCode;

    public void queryByHot() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("pageNum", PAGE_NUM);
        params.put("pageSize", PAGE_SIZE);

        ApiManager.getInstance()
                .queryByHot(params)
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<List<String>>(true, false, this) {
                    @Override
                    public void onSuccess(BaseResult<List<String>> result) {
                        tagHotLiveData.setValue(result.data);
                    }
                });

        gson = new Gson();
        if (null != userInfo) {
            userCode = userInfo.userCode;
        }
        String json = MMKVUtil.getString(CacheConstant.KEY_SEARCH_HISTORY + userCode);
        if (!TextUtils.isEmpty(json)) {
            List<String> list = gson.fromJson(json, new TypeToken<List<String>>() {
            }.getType());
            tagHistoryLiveData.setValue(list);
        }

        isShowTag.setValue(true);
    }

    public void search() {
        if (!TextUtils.isEmpty(searchContent.getValue())) {
            if (null != searchContent.getValue() && searchContent.getValue().trim().isEmpty()) {
                ToastUtils.showToast("请输入关键词！", Toast.LENGTH_SHORT);
                return;
            }
            isShowEmptyView.setValue(true);
            List<String> list = tagHistoryLiveData.getValue();
            if (null == list) {
                list = new ArrayList<>();
            }
            list.remove(searchContent.getValue());
            list.add(0, searchContent.getValue());
            if (list.size() > 6) {
                list.remove(list.size() - 1);
            }
            tagHistoryLiveData.setValue(list);
            if (null != userInfo) {
                userCode = userInfo.userCode;
            }
            MMKVUtil.save(CacheConstant.KEY_SEARCH_HISTORY + userCode, gson.toJson(list));
            ApiManager.getInstance()
                    .query(searchContent.getValue())
                    .compose(new ApiTransformer<>())
                    .subscribeWith(new ApiSubscriber<SearchEntity>(false, false, this) {
                        @Override
                        public void onSuccess(BaseResult<SearchEntity> result) {
                            List<SearchEntity> list3 = new ArrayList<>();
                            if (null != result.data.applyDtos && !result.data.applyDtos.isEmpty()) {
                                List<SearchEntranceEntity> searchEntranceEntityList = new ArrayList<>();
                                searchEntranceEntityList.add(new SearchEntranceEntity());
                                searchEntranceEntityList.addAll(result.data.applyDtos);
                                list3.add(new SearchEntity(1, searchEntranceEntityList, null));
                            }
                            if (null != result.data.informationDtos && !result.data.informationDtos.isEmpty()) {
                                List<SearchInfoEntity> searchInfoEntityList = new ArrayList<>();
                                searchInfoEntityList.add(new SearchInfoEntity());
                                searchInfoEntityList.addAll(result.data.informationDtos);
                                list3.add(new SearchEntity(2, null, searchInfoEntityList));
                            }
                            searchEntityList.setValue(list3);
                        }
                    });
        } else {
            ToastUtils.showToast("请输入关键词！", Toast.LENGTH_SHORT);
        }
    }

    public void back() {
        popPage();
    }
}
