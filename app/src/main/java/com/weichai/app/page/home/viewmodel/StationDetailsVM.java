package com.weichai.app.page.home.viewmodel;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.baidu.mapapi.model.LatLng;
import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.utils.StringUtil;
import com.weichai.app.bean.ServiceStationEntity;

import dev.utils.app.AppUtils;
import dev.utils.app.toast.ToastUtils;

public class StationDetailsVM extends BaseViewModel {
    public UnPeekLiveData<ServiceStationEntity> serviceStationEntity = new UnPeekLiveData<>();

    public void back() {
        popPage();
    }

    /**
     * 导航
     */
    public void navigation() {
        try {
            LatLng startLatLng = new LatLng(39.940387, 116.29446);
            LatLng endLatLng = new LatLng(Double.parseDouble(serviceStationEntity.getValue().dimension)
                    , Double.parseDouble(serviceStationEntity.getValue().longitude));
            String uri = String.format("baidumap://map/direction?origin=%s,%s&destination=" +
                            "%s,%s&mode=driving&src=com.34xian.demo", startLatLng.latitude, startLatLng.longitude,
                    endLatLng.latitude, endLatLng.longitude);
            Intent intent = new Intent();
            intent.setData(Uri.parse(uri));
            AppUtils.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            ToastUtils.showShort("请安装百度地图！", Toast.LENGTH_SHORT);
        }
    }

    /**
     * 立即维修
     */
    public void repairImmediately() {
        navigate(R.id.ServiceRepairImmediatelyFragment);
    }

    /**
     * 预约维修
     */
    public void bespeakMaintain() {

    }

    /**
     * 拨打电话
     *
     * @param phoneNum 电话号码
     */
    public void callPhone(String phoneNum) {
        StringUtil.callPhone(phoneNum);
    }
}
