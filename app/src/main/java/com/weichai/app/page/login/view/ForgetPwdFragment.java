package com.weichai.app.page.login.view;

import android.util.Log;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentForgetPwdBinding;
import com.weichai.app.page.login.viewmodel.ForgetPwdVM;
import com.weichai.app.page.login.viewmodel.LoginVM;

public class ForgetPwdFragment extends BaseFragment<FragmentForgetPwdBinding, ForgetPwdVM> {


    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_forget_pwd;
    }
}