package com.weichai.app.page.login.view;

import androidx.activity.OnBackPressedCallback;

import com.weichai.app.BR;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.arch.config.CacheConstant;
import com.weichai.app.arch.utils.MMKVUtil;
import com.weichai.app.databinding.FragmentLoginBinding;
import com.weichai.app.page.login.viewmodel.LoginVM;
import com.weichai.app.page.mine.UserCenterVM;

import java.util.Objects;

public class LoginFragment extends BaseFragment<FragmentLoginBinding, LoginVM> {


    @Override
    protected void initView() {
        UserCenterVM centerVM = getViewModelOfApp(UserCenterVM.class);
        mViewBinding.setVariable(BR.userVM, centerVM);
        centerVM.wxLoginCodeData
                .observe(this, code -> {
                    mViewModel.loginWxByCode(code);
                });

        if (centerVM.userInfo.get() != null) {
            mViewModel.textPhone.set(Objects.requireNonNull(centerVM.userInfo.get()).userName);
        }

    }

    @Override
    protected void initData() {
        //禁止返回按钮
        mActivity.getOnBackPressedDispatcher()
                .addCallback(this, new OnBackPressedCallback(true) {
                    @Override
                    public void handleOnBackPressed() {

                    }
                });

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_login;
    }
}