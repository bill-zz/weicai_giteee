package com.weichai.app.page.login.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentVerifyCodeLoginBinding;
import com.weichai.app.page.login.viewmodel.VerifyCodeVM;

/**
 * @ClassName: VerifyCodeFragment
 * @Description: 验证码登录
 * @Author: asanant
 * @Date: 2021/11/19 9:45 上午
 */
public class VerifyCodeFragment extends BaseFragment<FragmentVerifyCodeLoginBinding, VerifyCodeVM> {
    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_verify_code_login;
    }
}
