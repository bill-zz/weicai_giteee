package com.weichai.app.page.main;

import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.navigation.NavigationBarView;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.arch.utils.NetworkUtil;
import com.weichai.app.databinding.FragmentMainBinding;

import cn.jzvd.Jzvd;
import dev.utils.app.toast.ToastUtils;

/**
 * @ClassName: MainFragment
 * @Description:
 * @Author: 祖安
 * @Date: 2021/8/31 10:34 上午
 */
public class MainFragment extends BaseFragment<FragmentMainBinding, MainVM> {
    private final int[] itemIds = {R.id.home_page, R.id.mall, R.id.service, R.id.mine};

    @Override
    protected void initView() {
        getViewModelOfActivity(MainModel.class).currentItem.observe(mActivity, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                mViewBinding.vpContent.setCurrentItem(integer);
            }
        });
        mViewBinding.vpContent.setUserInputEnabled(false);
        mViewBinding.vpContent.setOffscreenPageLimit(4);
        mViewBinding.vpContent.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                mViewBinding.navBottom.setSelectedItemId(itemIds[position]);
            }
        });
        mViewBinding.navBottom.setItemIconTintList(null);
        // 取消长按显示toast
        View view = mViewBinding.navBottom.getChildAt(0);
        view.findViewById(R.id.home_page).setOnLongClickListener(v -> true);
        view.findViewById(R.id.mall).setOnLongClickListener(v -> true);
        view.findViewById(R.id.service).setOnLongClickListener(v -> true);
        view.findViewById(R.id.mine).setOnLongClickListener(v -> true);

        mViewBinding.navBottom.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                reset();
                switch (item.getItemId()) {
                    case R.id.home_page:
                        item.setIcon(R.mipmap.ic_tab_home_page_selected);
                        mViewBinding.vpContent.setCurrentItem(0);
                        break;
                    case R.id.mall:
                        if (!NetworkUtil.isNetworkConnected(mActivity)) {
                            ToastUtils.showToast("网络异常！", Toast.LENGTH_SHORT);
                        }
                        item.setIcon(R.mipmap.ic_tab_mall_selected);
                        mViewBinding.vpContent.setCurrentItem(1);
                        break;
                    case R.id.service:
                        item.setIcon(R.mipmap.ic_tab_service_selected);
                        mViewBinding.vpContent.setCurrentItem(2);
                        break;
                    case R.id.mine:
                        item.setIcon(R.mipmap.ic_tab_mine_selected);
                        mViewBinding.vpContent.setCurrentItem(3);
                        break;
                }
                return true;
            }
        });

    }

    private void reset() {
        MenuItem homePge = mViewBinding.navBottom.getMenu().findItem(R.id.home_page);
        MenuItem mall = mViewBinding.navBottom.getMenu().findItem(R.id.mall);
        MenuItem service = mViewBinding.navBottom.getMenu().findItem(R.id.service);
        MenuItem mine = mViewBinding.navBottom.getMenu().findItem(R.id.mine);
        homePge.setIcon(R.mipmap.ic_tab_home_page_normal);
        mall.setIcon(R.mipmap.ic_tab_mall_normal);
        service.setIcon(R.mipmap.ic_tab_service_normal);
        mine.setIcon(R.mipmap.ic_tab_mine_normal);
    }

    @Override
    protected void initData() {
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Jzvd.releaseAllVideos();
    }
}
