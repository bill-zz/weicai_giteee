package com.weichai.app.page.mine;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;

/**
 * @ClassName: MineVM
 * @Description:
 * @Author: 祖安
 * @Date: 2021/9/1 5:05 下午
 */
public class MineVM extends BaseViewModel {


    public void loginUser(){
//        navigate(R.id.login_page);
    }


    public void mineInfo(){
        navigate(R.id.mine_info);
    }

    public void navigateMineEngine(){
        navigate(R.id.mine_engine);
    }

    public void navigateMineIntegral(){
        navigate(R.id.mine_integral);
    }

    public void navigateMsg(){
        navigate(R.id.mine_msg);
    }
    public void navigateOrder(){
        navigate(R.id.mine_order);
    }
    public void navigateCoupons(){
        navigate(R.id.mine_coupons);
    }
    public void navigateRepairTrack(){
        navigate(R.id.repair_track);
    }
    public void navigateComplain(){
        navigate(R.id.mine_complain);
    }
    public void navigateSetting(){
        navigate(R.id.mine_setting);
    }

}
