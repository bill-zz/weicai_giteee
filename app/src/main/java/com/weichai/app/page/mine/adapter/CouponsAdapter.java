package com.weichai.app.page.mine.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.weichai.app.R;
import com.weichai.app.bean.MineCouponsEntity;
import com.weichai.app.bean.MineOrderEntity;
import com.weichai.app.databinding.ItemMineCouponsBinding;
import com.weichai.app.databinding.ItemMineOrderBinding;

/**
 * create by bill on 6.12.21
 */
public class CouponsAdapter extends BaseQuickAdapter<MineCouponsEntity, BaseDataBindingHolder<ItemMineCouponsBinding>> {

    public CouponsAdapter() {
        super(R.layout.item_mine_coupons);
    }


    @Override
    protected void convert(@NonNull BaseDataBindingHolder<ItemMineCouponsBinding> holder, MineCouponsEntity couponsEntity) {
        ItemMineCouponsBinding dataBinding = holder.getDataBinding();
        if (dataBinding != null) {
            dataBinding.tvEngineNo.setText(couponsEntity.getEngineNumber());
        }
    }
}
