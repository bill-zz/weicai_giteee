package com.weichai.app.page.mine.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.MineMsgEntity;
import com.weichai.app.databinding.ItemMineCouponsBinding;
import com.weichai.app.databinding.ItemMsgFaulBinding;

/**
 * create by bill on 6.12.21
 */
public class FaultMsgAdapter extends BaseQuickAdapter<MineMsgEntity, BaseDataBindingHolder<ItemMsgFaulBinding>> {

    public FaultMsgAdapter() {
        super(R.layout.item_msg_faul);
    }


    @Override
    protected void convert(@NonNull BaseDataBindingHolder<ItemMsgFaulBinding> holder, MineMsgEntity mineMsgEntity) {
        ItemMsgFaulBinding dataBinding = holder.getDataBinding();
        if (dataBinding != null) {
            dataBinding.tvTitle.setText(mineMsgEntity.getTitle());
            dataBinding.tvTime.setText(mineMsgEntity.getTime());
        }
    }
}
