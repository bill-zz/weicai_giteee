package com.weichai.app.page.mine.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.EngineEntity;
import com.weichai.app.databinding.ItemMineEngineBinding;
import com.weichai.app.databinding.ItemMsgFaulBinding;
import com.weichai.app.views.dialog.EditDialog;

/**
 * create by bill on 4.12.21
 */
public class MineEngineAdapter extends BaseQuickAdapter<EngineEntity, BaseDataBindingHolder<ItemMineEngineBinding>> {


    public MineEngineAdapter() {
        super(R.layout.item_mine_engine);
    }


    @Override
    protected void convert(@NonNull BaseDataBindingHolder<ItemMineEngineBinding> holder, EngineEntity engineEntity) {
        ItemMineEngineBinding dataBinding = holder.getDataBinding();
        if (dataBinding != null) {
            String engineNo = getContext().getString(R.string.no_engine);
            dataBinding.tvEngineNo.setText(engineNo + engineEntity.getNo());

            dataBinding.tvBind.setOnClickListener(v -> {
                if (iClickBack != null) {
                    iClickBack.navigate("bind_engine");
                }
            });

            dataBinding.tvEdit.setOnClickListener(v -> {
                if (iClickBack != null) {
                    iClickBack.navigate("edit_engine");
                }
            });
        }
    }

    public interface IClickBack {
        void navigate(String data);
    }

    private IClickBack iClickBack;

    public void setClickCallback(IClickBack iClickBack) {
        this.iClickBack = iClickBack;
    }
}
