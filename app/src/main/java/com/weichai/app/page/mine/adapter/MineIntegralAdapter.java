package com.weichai.app.page.mine.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.IntegralEntity;
import com.weichai.app.databinding.ItemMineEngineBinding;
import com.weichai.app.databinding.ItemMineIntegralBinding;

/**
 * create by bill on 4.12.21
 */
public class MineIntegralAdapter extends BaseQuickAdapter<IntegralEntity, BaseDataBindingHolder<ItemMineIntegralBinding>> {


    public MineIntegralAdapter() {
        super(R.layout.item_mine_integral);
    }


    @Override
    protected void convert(@NonNull BaseDataBindingHolder<ItemMineIntegralBinding> holder, IntegralEntity integralEntity) {
        ItemMineIntegralBinding dataBinding = holder.getDataBinding();
        if (dataBinding != null) {
            dataBinding.tvType.setText( integralEntity.getName());
            dataBinding.tvTime.setText( integralEntity.getTime());
            dataBinding.tvMoney.setText( integralEntity.getMoney());
        }
    }
}
