package com.weichai.app.page.mine.adapter;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.weichai.app.R;
import com.weichai.app.bean.MineMsgEntity;
import com.weichai.app.databinding.ItemMineOrderBinding;
import com.weichai.app.databinding.ItemMsgSysBinding;

/**
 * create by bill on 6.12.21
 */
public class SysMsgAdapter extends BaseQuickAdapter<MineMsgEntity, BaseDataBindingHolder<ItemMsgSysBinding>> {

    public SysMsgAdapter() {
        super(R.layout.item_msg_sys);
    }


    @Override
    protected void convert(@NonNull BaseDataBindingHolder<ItemMsgSysBinding> holder, MineMsgEntity mineMsgEntity) {
        ItemMsgSysBinding dataBinding = holder.getDataBinding();
        if (dataBinding != null) {
            dataBinding.setData(mineMsgEntity);
        }
    }
}
