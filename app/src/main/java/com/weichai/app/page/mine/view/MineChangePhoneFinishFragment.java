package com.weichai.app.page.mine.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineChangePhoneFinishBinding;
import com.weichai.app.databinding.FragmentMineChangePhoneNewBinding;
import com.weichai.app.page.mine.viewmodel.MineChangePhoneNewVM;
import com.weichai.app.page.mine.viewmodel.MineChangePhoneVM;

/**
 * 修改新手机号
 *
 */
public class MineChangePhoneFinishFragment extends BaseFragment<FragmentMineChangePhoneFinishBinding, MineChangePhoneVM> {

    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_change_phone_finish;
    }
}
