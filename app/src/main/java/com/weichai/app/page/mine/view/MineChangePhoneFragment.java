package com.weichai.app.page.mine.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineChangePhoneBinding;
import com.weichai.app.page.mine.viewmodel.MineChangePhoneVM;

/**
 * 修改手机号认证
 *
 */
public class MineChangePhoneFragment extends BaseFragment<FragmentMineChangePhoneBinding, MineChangePhoneVM> {

    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_change_phone;
    }
}
