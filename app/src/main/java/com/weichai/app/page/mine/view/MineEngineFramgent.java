package com.weichai.app.page.mine.view;

import android.widget.ImageView;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.arch.config.RouterParams;
import com.weichai.app.bean.EngineEntity;
import com.weichai.app.databinding.FragmentMineEngineBinding;
import com.weichai.app.page.mine.adapter.ItemDecoration;
import com.weichai.app.page.mine.adapter.MineEngineAdapter;
import com.weichai.app.page.mine.viewmodel.MineEngineVM;

import java.util.List;

/**
 * create by bill on 4.12.21
 */
public class MineEngineFramgent extends BaseFragment<FragmentMineEngineBinding, MineEngineVM> {


    private final MineEngineAdapter adapter = new MineEngineAdapter();

    @Override
    protected void initView() {
        int decoration = requireContext().getResources().getDimensionPixelSize(R.dimen.dp_15);
        mViewBinding.rv.addItemDecoration(new ItemDecoration(0, 0, 0, decoration));
        mViewBinding.rv.setLayoutManager(new LinearLayoutManager(requireContext()));
        mViewBinding.rv.setAdapter(adapter);
        ImageView imageView = new ImageView(requireContext());
        imageView.setImageResource(R.mipmap.ic_add_app);
        adapter.addFooterView(imageView);
    }

    @Override
    protected void initData() {

        mViewModel.fetchEngineData();
        mViewModel.engineData.observe(this, (Observer<List<EngineEntity>>) adapter::setList);

        adapter.setClickCallback(data -> {
            if(data.equals("bind_engine")){
                navigate(new RouterParams(R.id.MineEngineWalkFragment));
            }else {
                navigate(new RouterParams(R.id.EditEngineFragment));
            }
        });
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_engine;
    }
}
