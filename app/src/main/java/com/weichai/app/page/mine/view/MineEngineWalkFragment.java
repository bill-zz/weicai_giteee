package com.weichai.app.page.mine.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineEngineWalkBinding;
import com.weichai.app.page.mine.viewmodel.MineEngineWalkVM;

/**
 * 绑定智多行
 *
 */
public class MineEngineWalkFragment extends BaseFragment<FragmentMineEngineWalkBinding, MineEngineWalkVM> {

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_engine_walk;
    }
}
