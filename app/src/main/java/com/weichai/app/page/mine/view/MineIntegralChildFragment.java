package com.weichai.app.page.mine.view;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.IntegralEntity;
import com.weichai.app.databinding.FragmentMineIntegralChildBinding;
import com.weichai.app.page.mine.adapter.MineIntegralAdapter;
import com.weichai.app.page.mine.viewmodel.MineIntegralVM;

import java.util.List;

/**
 * create by bill on 4.12.21
 */
public class MineIntegralChildFragment extends BaseFragment<FragmentMineIntegralChildBinding, MineIntegralVM> {

    private final MineIntegralAdapter adapter = new MineIntegralAdapter();

    @Override
    protected void initView() {
        mViewBinding.rv.setLayoutManager(new LinearLayoutManager(requireContext()));
        mViewBinding.rv.setAdapter(adapter);
    }

    @Override
    protected void initData() {
        mViewModel.fetchIntegralData();
        mViewModel.integralData.observe(this, (Observer<List<IntegralEntity>>) adapter::setList);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_integral_child;
    }
}
