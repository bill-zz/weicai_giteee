package com.weichai.app.page.mine.view;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.MineMsgEntity;
import com.weichai.app.databinding.FragmentMineChildMsgBinding;
import com.weichai.app.databinding.FragmentMineMsgBinding;
import com.weichai.app.page.mine.adapter.FaultMsgAdapter;
import com.weichai.app.page.mine.adapter.ItemDecoration;
import com.weichai.app.page.mine.adapter.SysMsgAdapter;
import com.weichai.app.page.mine.viewmodel.MineMsgVM;

import java.util.ArrayList;
import java.util.List;

/**
 * create by bill on 6.12.21
 */
public class MineMsgFaulFragment extends BaseFragment<FragmentMineChildMsgBinding, MineMsgVM> {

    private final FaultMsgAdapter adapter=new FaultMsgAdapter();

    @Override
    protected void initView() {
        mViewBinding.rv.setLayoutManager(new LinearLayoutManager(requireContext()));
        mViewBinding.rv.setAdapter(adapter);

    }

    @Override
    protected void initData() {
        mViewModel.fetchData();
        mViewModel.data.observe(this, new Observer<List<MineMsgEntity>>() {
            @Override
            public void onChanged(List<MineMsgEntity> list) {
                adapter.setList(list);
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_child_msg;
    }
}
