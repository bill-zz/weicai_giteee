package com.weichai.app.page.mine.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMinePersonBinding;
import com.weichai.app.page.mine.viewmodel.MinePersonVM;
import com.weichai.app.views.dialog.EditDialog;
import com.weichai.app.views.dialog.UserTypeBottomDialog;

/**
 * 用户信息
 */
public class MinePersonFragment extends BaseFragment<FragmentMinePersonBinding, MinePersonVM> {

    @Override
    protected void initView() {
        mViewBinding.setFg(this);
    }

    @Override
    protected void initData() {
    }

    public void showIdCardDialog() {
        EditDialog editDialog = new EditDialog(requireContext());
        editDialog.setTitle("编辑身份证号");
        editDialog.setMsgHint("请输入身份证号…");
        editDialog.setMaxSize(18);
        editDialog.show();
        editDialog.setClickOkCallback(msg -> {
            mViewBinding.setIdCard(msg);
        });
    }

    public void showAddressDialog() {
        EditDialog editDialog = new EditDialog(requireContext());
        editDialog.setTitle("编辑联系地址");
        editDialog.setMsgHint("请输入地址…");
        editDialog.setMaxSize(50);
        editDialog.show();
        editDialog.setClickOkCallback(msg -> {
            mViewBinding.setAddress(msg);
        });
    }

    public void showTypeDialog() {
        UserTypeBottomDialog typeDialog = new UserTypeBottomDialog(requireContext());
        typeDialog.show();
        typeDialog.setClickOkCallback(type -> {

        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_person;
    }
}
