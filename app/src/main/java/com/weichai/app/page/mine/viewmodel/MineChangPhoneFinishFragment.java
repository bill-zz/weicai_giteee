package com.weichai.app.page.mine.viewmodel;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentMineChangePhoneFinishBinding;

/**
 * create by bill on 8.12.21
 */
public class MineChangPhoneFinishFragment extends BaseFragment<FragmentMineChangePhoneFinishBinding, MineChangePhoneVM> {

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_mine_change_phone_finish;
    }
}
