package com.weichai.app.page.mine.viewmodel;


import android.util.Log;
import android.view.View;

import androidx.databinding.ObservableField;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.bean.ChangePhoneReq;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Observable;

/**
 * 用户修改信息的ViewModel
 */
public class MineChangePhoneNewVM extends BaseViewModel {


    public ObservableField<String> textAuthBtn = new ObservableField<>("获取验证码");//获取验证按钮
    public ObservableField<Boolean> enableAuthBtn = new ObservableField<>(true);//是否可用获取验证码按钮
    public ObservableField<Boolean> enableCommitBtn = new ObservableField<>(false);//是否可用提交按钮
    public ObservableField<Integer> phoneErrVisible = new ObservableField<>(View.GONE);//

    private String newPhone = "";
    private String code = "";

    public MineChangePhoneNewVM() {
    }

    public void sendVerify() {
        ApiManager
                .getInstance()
                .sendVerify(newPhone, "5")
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<String>(true, true, this) {
                    @Override
                    public void onSuccess(BaseResult<String> result) {
                        intervalAuthCode();
                        showToast("发送成功");
                    }
                });
    }

    /**
     * 提交修改手机号
     */
    public void commitChangePhone() {

//        HashMap<String, Object> params = new HashMap<>();
//        params.put("userName", userName.get());
//        params.put("verificationCode", verifyCode.get());

        ChangePhoneReq changePhoneReq = new ChangePhoneReq();
        changePhoneReq.setUserName(newPhone);
        changePhoneReq.setVerificationCode(code);

        Log.i("weicai", "commitChangePhone: " + changePhoneReq.toString());

        ApiManager.getInstance()
                .changePhone(changePhoneReq)
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<String>(true, true, this) {
                    @Override
                    public void onSuccess(BaseResult<String> result) {
                        showToast(result.data);
                        navigateChangePhoneFinishPage();
                    }
                });
    }

    public void navigateChangePhoneFinishPage() {
        navigate(R.id.mine_change_phone_finish);
    }


    //文字变化监听
    public void onTextChangedPhone(CharSequence value, int i, int i1, int i2) {
        newPhone = value.toString();
        if (newPhone.length() == 11 && code.length() == 6) {
            enableCommitBtn.set(true);
        } else {
            enableCommitBtn.set(false);
        }
    }


    //文字变化监听
    public void onTextChanged(CharSequence value, int i, int i1, int i2) {
        code = value.toString();
        if (newPhone.length() == 11 && code.length() == 6) {
            enableCommitBtn.set(true);
        } else {
            enableCommitBtn.set(false);
        }
    }

    /**
     * 验证码倒计时
     */
    private void intervalAuthCode() {
        Observable
                .interval(1, 1, TimeUnit.SECONDS)
                .take(61)
                .subscribe(value -> {
                    int time = (int) (60 - value);
                    if (time == 0) {
                        textAuthBtn.set(String.format("重新获取验证码", time));
                        enableAuthBtn.set(true);
                    } else {
                        textAuthBtn.set(String.format("%sS后重新获取", time));
                        enableAuthBtn.set(false);
                    }
                });
    }


}
