package com.weichai.app.page.mine.viewmodel;

import android.util.Log;

import androidx.databinding.ObservableField;

import com.kunminx.architecture.ui.callback.UnPeekLiveData;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.http.ApiManager;
import com.weichai.app.arch.http.ApiSubscriber;
import com.weichai.app.arch.http.ApiTransformer;
import com.weichai.app.bean.BaseResult;
import com.weichai.app.page.mine.UserCenterVM;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Observable;

/**
 * 用户修改信息的ViewModel
 */
public class MineChangePhoneVM extends BaseViewModel {


    public UnPeekLiveData<String> userName = new UnPeekLiveData<>();
    public ObservableField<String> verifyCode = new ObservableField<>();
    public ObservableField<String> textAuthBtn = new ObservableField<>("获取验证码");//获取验证按钮
    public ObservableField<Boolean> enableAuthBtn = new ObservableField<>(true);//是否可用获取验证码按钮
    public ObservableField<Boolean> enableCommitBtn = new ObservableField<>(false);//是否可用提交按钮


    public MineChangePhoneVM() {
        ///绑定全局用户信息数据
        this.userName.setValue(Objects.requireNonNull(getViewModelOfApp(UserCenterVM.class).userInfo.get()).userName);
    }


    public void sendVerify() {
        ApiManager
                .getInstance()
                .sendVerify(userName.getValue(), "5")
                .compose(new ApiTransformer<>())
                .subscribeWith(new ApiSubscriber<String>(true, true, this) {
                    @Override
                    public void onSuccess(BaseResult<String> result) {
                        intervalAuthCode();
                        showToast("发送成功");
                    }
                });
    }


    public void commitChangePhone() {
        navigate(R.id.mine_change_phone_new);
    }


    //文字变化监听
    public void onTextChanged(CharSequence value, int i, int i1, int i2) {
        ///延迟获取最新输入值
        Observable.just(1)
                .delay(50, TimeUnit.MILLISECONDS)
                .subscribe(integer -> {
                    Log.d("MineChangePhoneVM", "i:" + i + "  i1:" + i1 + "   i2:" + i2);
                    if (i == 5) {
                        enableCommitBtn.set(true);
                    } else {
                        enableCommitBtn.set(false);
                    }
                });
    }

    /**
     * 验证码倒计时
     */
    private void intervalAuthCode() {
        Observable
                .interval(1, 1, TimeUnit.SECONDS)
                .take(61)
                .subscribe(value -> {
                    int time = (int) (60 - value);
                    if (time == 0) {
                        textAuthBtn.set(String.format("重新获取验证码", time));
                        enableAuthBtn.set(true);
                    } else {
                        textAuthBtn.set(String.format("%sS后重新获取", time));
                        enableAuthBtn.set(false);
                    }
                });
    }


    public void navigateHome() {
        popPage(true);
    }

}
