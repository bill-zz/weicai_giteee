package com.weichai.app.page.mine.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.EngineEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * create by bill on 4.12.21
 */
public class MineEngineVM extends BaseViewModel {

    public MutableLiveData engineData = new  MutableLiveData<List<EngineEntity>>();


    public void fetchEngineData() {
        test();
    }

    private void test() {
        ArrayList<EngineEntity> entities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            entities.add(new EngineEntity(i + "", ""));
        }
        engineData.setValue(entities);
    }

}
