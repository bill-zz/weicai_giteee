package com.weichai.app.page.mine.viewmodel;


import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;

/**
 * 用户信息的ViewModel
 *
 */
public class MineInfoVM extends BaseViewModel {



    public void changePhone(){
        navigate(R.id.mine_change_phone);
    }

    public MineInfoVM(){

    }

}
