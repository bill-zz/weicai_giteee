package com.weichai.app.page.mine.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.EngineEntity;
import com.weichai.app.bean.IntegralEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * create by bill on 4.12.21
 */
public class MineIntegralVM extends BaseViewModel {

    public MutableLiveData integralData = new  MutableLiveData<List<EngineEntity>>();


    public void fetchIntegralData() {
        test();
    }

    private void test() {
        List<IntegralEntity> entities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            entities.add(new IntegralEntity("潍柴商城积分兑换商品","2021-10-10","-1600"));
        }
        integralData.setValue(entities);
    }

}
