package com.weichai.app.page.mine.viewmodel;


import androidx.lifecycle.MutableLiveData;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.EngineEntity;
import com.weichai.app.bean.MineMsgEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户信息的ViewModel
 */
public class MineMsgVM extends BaseViewModel {


    public MutableLiveData data = new MutableLiveData<List<MineMsgEntity>>();

    public void fetchData() {
        test();
    }

    private void test() {
        ArrayList<MineMsgEntity> entities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            entities.add(new MineMsgEntity("", "title" + i, "msg", "2012-12-06"));
        }
        data.setValue(entities);
    }

}
