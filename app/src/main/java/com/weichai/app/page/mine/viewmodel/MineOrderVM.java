package com.weichai.app.page.mine.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.MineMsgEntity;
import com.weichai.app.bean.MineOrderEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * create by bill on 7.12.21
 */
public class MineOrderVM extends BaseViewModel {

    public MutableLiveData data = new MutableLiveData<List<MineOrderEntity>>();

    public void fetchData() {
        test();
    }

    private void test() {
        ArrayList<MineOrderEntity> entities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            entities.add(new MineOrderEntity("SP2111170001", "A115647473535163" + i, "潍坊市第一销售公司", "2012-12-07"));
        }
        data.setValue(entities);
    }
}
