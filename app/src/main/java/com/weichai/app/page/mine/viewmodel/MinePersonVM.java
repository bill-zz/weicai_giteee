package com.weichai.app.page.mine.viewmodel;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;

/**
 * create by bill on 7.12.21
 */
public class MinePersonVM extends BaseViewModel {


    public void changePhone(){
        navigate(R.id.mine_change_phone);
    }
}
