package com.weichai.app.page.service.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentCarCheckBinding;
import com.weichai.app.page.service.viewmodel.CarCheckVM;

public class CarCheckFragment extends BaseFragment<FragmentCarCheckBinding, CarCheckVM> {

    @Override
    protected void initView() {
        mViewModel.init();
    }

    @Override
    protected void initData() {
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_car_check;
    }
}
