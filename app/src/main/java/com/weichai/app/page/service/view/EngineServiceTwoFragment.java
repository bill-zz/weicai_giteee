package com.weichai.app.page.service.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentEngineServiceBinding;
import com.weichai.app.databinding.FragmentEngineServiceTwoBinding;
import com.weichai.app.page.service.viewmodel.ServiceVM;

public class EngineServiceTwoFragment extends BaseFragment<FragmentEngineServiceTwoBinding, ServiceVM> {

    @Override
    protected void initView() {


    }

    @Override
    protected void initData() {
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_engine_service_two;
    }
}
