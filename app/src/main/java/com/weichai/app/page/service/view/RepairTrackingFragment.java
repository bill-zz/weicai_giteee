package com.weichai.app.page.service.view;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.RepairTrackEntity;
import com.weichai.app.databinding.FragmentRepairTrackingBinding;
import com.weichai.app.page.mine.adapter.ItemDecoration;
import com.weichai.app.page.service.adapter.RepairTrackAdapter;
import com.weichai.app.page.service.viewmodel.RepairTrackVM;

import java.util.List;


public class RepairTrackingFragment extends BaseFragment<FragmentRepairTrackingBinding, RepairTrackVM> {

    private final RepairTrackAdapter adapter = new RepairTrackAdapter();


    @Override
    protected void initView() {
        int decoration = requireContext().getResources().getDimensionPixelSize(R.dimen.dp_15);
        mViewBinding.rv.addItemDecoration(new ItemDecoration(0, 0, 0, decoration));
        mViewBinding.rv.setLayoutManager(new LinearLayoutManager(requireContext()));
        mViewBinding.rv.setAdapter(adapter);
    }

    @Override
    protected void initData() {
        mViewModel.fetchRepairTrackData();
        mViewModel.repairTrackListData.observe(this, new Observer<List<RepairTrackEntity>>(){
            @Override
            public void onChanged(List<RepairTrackEntity> data) {
                adapter.setList(data);
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_repair_tracking;
    }
}
