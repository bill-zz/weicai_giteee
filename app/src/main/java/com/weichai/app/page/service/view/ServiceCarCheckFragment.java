package com.weichai.app.page.service.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentServiceCarCheckBinding;
import com.weichai.app.page.service.viewmodel.CarCheckVM;


public class ServiceCarCheckFragment extends BaseFragment<FragmentServiceCarCheckBinding, CarCheckVM> {


    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_service_car_check;
    }
}
