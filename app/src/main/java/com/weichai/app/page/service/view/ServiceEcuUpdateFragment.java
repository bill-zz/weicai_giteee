package com.weichai.app.page.service.view;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentServiceEcuUpdateBinding;
import com.weichai.app.page.service.viewmodel.EcuUpdateVM;


public class ServiceEcuUpdateFragment extends BaseFragment<FragmentServiceEcuUpdateBinding, EcuUpdateVM> {
    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_service_ecu_update;
    }
}
