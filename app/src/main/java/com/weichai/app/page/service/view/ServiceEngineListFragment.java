package com.weichai.app.page.service.view;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.bean.EngineEntity;
import com.weichai.app.databinding.FragmentServiceEngineListBinding;
import com.weichai.app.page.mine.adapter.ItemDecoration;
import com.weichai.app.page.service.adapter.EngineListAdapter;
import com.weichai.app.page.service.viewmodel.EngineListVM;

import java.util.List;


public class ServiceEngineListFragment extends BaseFragment<FragmentServiceEngineListBinding, EngineListVM> {

    private final EngineListAdapter adapter = new EngineListAdapter();


    @Override
    protected void initView() {
        int decoration = requireContext().getResources().getDimensionPixelSize(R.dimen.dp_15);
        mViewBinding.rv.addItemDecoration(new ItemDecoration(0, 0, 0, decoration));
        mViewBinding.rv.setLayoutManager(new LinearLayoutManager(requireContext()));
        mViewBinding.rv.setAdapter(adapter);

    }

    @Override
    protected void initData() {
        mViewModel.fetchEngineListData();
        mViewModel.engineListData.observe(this, new Observer<List<EngineEntity>>() {
            @Override
            public void onChanged(List<EngineEntity> data) {
                adapter.setList(data);
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_service_engine_list;
    }
}
