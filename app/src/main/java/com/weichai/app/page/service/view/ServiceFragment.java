package com.weichai.app.page.service.view;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentServiceBinding;
import com.weichai.app.page.service.viewmodel.ServiceVM;

import java.util.ArrayList;
import java.util.List;

public class ServiceFragment extends BaseFragment<FragmentServiceBinding, ServiceVM> {

    @Override
    protected void initView() {

        EngineServiceFragment service1 = new EngineServiceFragment();
        EngineServiceTwoFragment service2 = new EngineServiceTwoFragment();
        List<Fragment> fgList = new ArrayList();
        fgList.add(service1);
        fgList.add(service2);


        mViewBinding.vp.setAdapter(new FragmentStateAdapter(getChildFragmentManager(), getLifecycle()) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                return fgList.get(position);
            }

            @Override
            public int getItemCount() {
                return fgList.size();
            }
        });

        mViewBinding.vp.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                if (position == 0) {
                    mViewBinding.ivPage.setImageResource(R.mipmap.ic_server_bottom_left);
                } else {
                    mViewBinding.ivPage.setImageResource(R.mipmap.ic_server_bottom_right);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });
    }

    @Override
    protected void initData() {
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_service;
    }
}
