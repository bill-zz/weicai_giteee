package com.weichai.app.page.service.view;

import android.graphics.Color;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FramgentRepairImmediatelyBinding;
import com.weichai.app.page.service.viewmodel.RepairVM;


public class ServiceRepairImmediatelyFragment extends BaseFragment<FramgentRepairImmediatelyBinding, RepairVM> {
    @Override
    protected void initView() {


        mViewBinding.vUpkeep.setOnClickListener(v -> {
            mViewBinding.vUpkeep.setBackgroundResource(R.drawable.shape_select_enable);
            mViewBinding.ivUpkeep.setImageResource(R.mipmap.ic_engine_upkeep_enable);
            mViewBinding.tvUpkeep.setTextColor(Color.parseColor("#ffffffff"));

            mViewBinding.vFault.setBackgroundResource(R.drawable.shape_select_unable);
            mViewBinding.ivFault.setImageResource(R.mipmap.ic_engine_fault_unable);
            mViewBinding.tvFault.setTextColor(Color.parseColor("#d9000000"));
        });

        mViewBinding.vFault.setOnClickListener(v -> {

            mViewBinding.vFault.setBackgroundResource(R.drawable.shape_select_enable);
            mViewBinding.ivFault.setImageResource(R.mipmap.ic_engine_fault_enable);
            mViewBinding.tvFault.setTextColor(Color.parseColor("#ffffffff"));

            mViewBinding.vUpkeep.setBackgroundResource(R.drawable.shape_select_unable);
            mViewBinding.ivUpkeep.setImageResource(R.mipmap.ic_engine_upkeep_unable);
            mViewBinding.tvUpkeep.setTextColor(Color.parseColor("#d9000000"));
        });


    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.framgent_repair_immediately;
    }
}
