package com.weichai.app.page.service.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.EngineEntity;

import java.util.ArrayList;
import java.util.List;


public class EngineListVM extends BaseViewModel {

    public MutableLiveData engineListData = new MutableLiveData();


    public void fetchEngineListData() {
        test();
    }

    public void test() {
        List<EngineEntity> engineList = new ArrayList();
        for (int i = 0; i < 10; i++) {
            engineList.add(new EngineEntity(i + "", ""));
        }
        engineListData.postValue(engineList);
    }
}
