package com.weichai.app.page.service.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.bean.RepairTrackEntity;

import java.util.ArrayList;
import java.util.List;


public class RepairTrackVM extends BaseViewModel {

    public MutableLiveData repairTrackListData = new MutableLiveData();


    public void fetchRepairTrackData() {
        test();
    }

    public void test() {
        List<RepairTrackEntity> engineList = new ArrayList();
        for (int i = 0; i < 10; i++) {
            engineList.add(new RepairTrackEntity(i + "SP2111170001", "五岳汽车销售有限公司","2019-06-22"));
        }
        repairTrackListData.postValue(engineList);
    }

}
