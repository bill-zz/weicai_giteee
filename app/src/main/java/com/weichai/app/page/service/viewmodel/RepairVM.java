package com.weichai.app.page.service.viewmodel;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;
import com.weichai.app.arch.utils.StringUtil;


public class RepairVM extends BaseViewModel {

    public void navigateStation() {
        if (StringUtil.isLogin()) {
            navigate(R.id.serviceStationFragment);
        }
    }

}
