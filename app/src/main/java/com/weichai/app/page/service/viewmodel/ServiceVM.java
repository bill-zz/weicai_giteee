package com.weichai.app.page.service.viewmodel;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseViewModel;

public class ServiceVM extends BaseViewModel {

    public void navigateEngineList() {
        navigate(R.id.service_engine_list);
    }

    public void navigateCarCheck() {
        navigate(R.id.service_car_check);
    }
    public void navigateEcuUpdate() {
        navigate(R.id.ecp_update);
    }
    public void navigateRepairTrack() {
        navigate(R.id.repair_track);
    }

    public void navigateUpkeep() {
//        navigate(R.id.repair_track);
    }
    public void navigateCleanCode() {
        navigate(R.id.server_engint_check_result);
    }

    public void navigateAddEngine() {
        navigate(R.id.server_add_engine);
    }
}
