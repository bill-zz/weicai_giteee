package com.weichai.app.page.splash;

import androidx.activity.OnBackPressedCallback;

import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentSplashBinding;

/**
 * @ClassName: SplashFragment
 * @Description:启动页
 * @Author: 祖安
 * @Date: 2021/11/27 9:48 上午
 */
public class SplashFragment extends BaseFragment<FragmentSplashBinding, SplashVM> {
    @Override
    protected void initView() {
        mViewModel.startSkinCountDown();
    }

    @Override
    protected void initData() {
        //禁止返回按钮
        mActivity
                .getOnBackPressedDispatcher()
                .addCallback(this,new OnBackPressedCallback(true) {
                    @Override
                    public void handleOnBackPressed() {

                    }
                });
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_splash;
    }
}
