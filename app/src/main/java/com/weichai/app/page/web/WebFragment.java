package com.weichai.app.page.web;

import android.util.Log;

import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.weichai.app.R;
import com.weichai.app.arch.base.BaseFragment;
import com.weichai.app.databinding.FragmentWebBinding;

/**
 * @ClassName: WebFragment
 * @Description:
 * @Author: 祖安
 * @Date: 2021/8/31 11:07 上午
 */
public class WebFragment extends BaseFragment<FragmentWebBinding, WebVM> {
    @Override
    protected void initView() {
        if (null != getArguments()) {
            mViewBinding.webView.loadUrl((String) getArguments().get("url"));
        }
        mViewBinding.webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView webView, int progress) {
                super.onProgressChanged(webView, progress);
                Log.e("TAG", "onProgressChanged: " + progress);
            }

        });

        mViewBinding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                webView.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    protected void initData() {
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_web;
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewBinding.webView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mViewBinding.webView.onPause();
    }

    @Override
    public void onDestroyView() {
        mViewBinding.webView.destroy();
        super.onDestroyView();
    }

}
