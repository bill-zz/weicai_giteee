package com.weichai.app.views.cityselect.item;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.weichai.app.R;

import dev.utils.app.ResourceUtils;

public class CustomItemDecoration extends RecyclerView.ItemDecoration {

    private final int mTitleHeight;
    private final int mTitleTextSize;
    private final Paint mPaint;
    private final Paint mTextPaint;
    private final Rect textRect;
    private TitleDecorationCallback callback;
    private final Paint mGrayPaint;
    private float titlePaddingLeft;

    public CustomItemDecoration(Context context, TitleDecorationCallback callback) {
        this.callback = callback;
        mTitleHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 28, context.getResources().getDisplayMetrics());
        mTitleTextSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, context.getResources().getDisplayMetrics());
        titlePaddingLeft = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, context.getResources().getDisplayMetrics());
        mTextPaint = new Paint();
        mTextPaint.setTextSize(mTitleTextSize);
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(Color.parseColor("#73000000"));

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(ResourceUtils.getColor(R.color.color_FFF4F6F7));

        mGrayPaint = new Paint();
        mGrayPaint.setAntiAlias(true);
        mGrayPaint.setColor(Color.WHITE);

        textRect = new Rect();
    }

    // 这个方法用于给item隔开距离，类似直接给item设padding
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = ((RecyclerView.LayoutParams) view.getLayoutParams()).getViewLayoutPosition();

        if (position == 0) {
            outRect.top = 0;
        } else if (isFirst(position)) {
            outRect.top = mTitleHeight;
        } else {
            outRect.top = 0;
        }
    }

    // 这个方法用于给getItemOffsets()隔开的距离填充图形,
    // 在item绘制之前时被调用，将指定的内容绘制到item view内容之下；
    @Override
    public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {

        // 获取当前屏幕可见 item 数量，而不是 RecyclerView 所有的 item 数量
        int childCount = parent.getChildCount();
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();

        for (int i = 0; i < childCount; i++) {
            final View view = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view
                    .getLayoutParams();
            int position = params.getViewLayoutPosition();

            if(position==0){

            }else
            if (isFirst(position)) {
                float top = view.getTop() - mTitleHeight;
                float bottom = view.getTop();
                canvas.drawRect(left, top, right, bottom, mPaint);

                String groupName = callback.getGroupName(position);
                mTextPaint.getTextBounds(groupName, 0, groupName.length(), textRect);
                float x = view.getPaddingLeft() + titlePaddingLeft;
                float y = top + (mTitleHeight - textRect.height()) / 3 * 2 + textRect.height();
                canvas.drawText(callback.getGroupName(position), x, y, mTextPaint);
            } else {
                float top = view.getTop() - 1;
                float bottom = view.getTop();
                canvas.drawRect(left, top, right, bottom, mPaint);
            }
        }
    }

    /**
     * 判断是否是同一组的第一个item
     *
     * @param position
     * @return
     */
    private boolean isFirst(int position) {

        String prevGroupId = callback.getGroupId(position - 1);
        String groupId = callback.getGroupId(position);
        return !prevGroupId.equals(groupId);
    }

    public interface TitleDecorationCallback {

        String getGroupId(int position);

        String getGroupName(int position);
    }
}