package com.weichai.app.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.weichai.app.R;
import com.weichai.app.databinding.DialogEditBinding;
import com.weichai.app.databinding.DialogUserTypeBinding;

import kotlin.TuplesKt;

/**
 * create by bill on 8.12.21
 */
public class UserTypeBottomDialog extends Dialog {

    public UserTypeBottomDialog(@NonNull Context context) {
        super(context);
    }

    public UserTypeBottomDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected UserTypeBottomDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    private DialogUserTypeBinding dataBinding;

    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_user_type, null, false);
        dataBinding = DialogUserTypeBinding.bind(view);
        setContentView(dataBinding.getRoot());

        dataBinding.setDialog(this);


        WindowManager.LayoutParams params = getWindow().getAttributes();
        getWindow().getDecorView().setPadding(0, 0, 0, 0);
        getWindow().getDecorView().setBackgroundColor(Color.parseColor("#00ffffff"));
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);


        dataBinding.ivClose.setOnClickListener(v -> {
            dismiss();
        });
    }

    public void select(int type) {
        this.type = type;
        if (type == 0) {
            dataBinding.ivPerson.setVisibility(View.VISIBLE);
            dataBinding.ivDrive.setVisibility(View.GONE);
        } else {
            dataBinding.ivPerson.setVisibility(View.GONE);
            dataBinding.ivDrive.setVisibility(View.VISIBLE);
        }
    }

    public void clickOk() {
        dismiss();
        if (select != null) {
            select.onSelectClick(type);
        }
    }


    public interface ISelect {
        void onSelectClick(int type);
    }

    private ISelect select;

    public void setClickOkCallback(ISelect select) {
        select = select;
    }
}
