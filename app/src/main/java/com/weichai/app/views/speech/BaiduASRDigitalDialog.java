
package com.weichai.app.views.speech;

import android.annotation.SuppressLint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.speech.SpeechRecognizer;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.weichai.app.R;

/**
 * 语音识别对话框
 *
 * @author yangliang02
 */
@SuppressLint("Registered")
public class BaiduASRDigitalDialog extends BaiduASRDialog {
    private SDKAnimationView sdkAnimationView;
    private TextView tvTips;
    private TextView tvStopSpeech;
    private ConstraintLayout clNoMatch;
    private TextView tvStartRecognition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        startRecognition();
    }

    private void initView() {
        View mContentRoot = View.inflate(this,
                getResources().getIdentifier("bdspeech_digital_layout", "layout", getPackageName()), null);
        if (mContentRoot != null) {
            sdkAnimationView = mContentRoot.findViewById(R.id.SDKAnimationView);
            ImageView ivCancel = mContentRoot.findViewById(R.id.iv_cancel);
            tvStopSpeech = mContentRoot.findViewById(R.id.tv_stop_speech);
            clNoMatch = mContentRoot.findViewById(R.id.cl_no_match);
            tvStartRecognition = mContentRoot.findViewById(R.id.tv_start_recognition);
            tvTips = mContentRoot.findViewById(R.id.tv_tips);
            ivCancel.setOnClickListener(mClickListener);
            tvStopSpeech.setOnClickListener(mClickListener);
            tvStartRecognition.setOnClickListener(mClickListener);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(new View(this));
            ViewGroup.LayoutParams param = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            addContentView(mContentRoot, param);
        }
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);
    }

    private void stopRecognizingAnimation() {
        sdkAnimationView.resetAnimation();
    }

    private void startRecognizingAnimation() {
        sdkAnimationView.startRecognizingAnimation();
    }

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_cancel:
                    cancleRecognition();
                    finish();
                    break;
                case R.id.tv_stop_speech:
                    speakFinish();
                    finish();
                    break;
                case R.id.tv_start_recognition:
                    startRecognition();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onRecognitionStart() {
        tvTips.setVisibility(View.VISIBLE);
        sdkAnimationView.setVisibility(View.VISIBLE);
        tvStopSpeech.setVisibility(View.VISIBLE);
        clNoMatch.setVisibility(View.GONE);
        sdkAnimationView.startInitializingAnimation();
    }

    @Override
    protected void onPrepared() {
        sdkAnimationView.startPreparingAnimation();
    }

    @Override
    protected void onBeginningOfSpeech() {
        sdkAnimationView.startRecordingAnimation();
    }

    @Override
    protected void onVolumeChanged(float volume) {
        sdkAnimationView.setCurrentDBLevelMeter(volume);
    }

    @Override
    protected void onEndOfSpeech() {
        startRecognizingAnimation();
    }

    @Override
    protected void onFinish(int errorType, int errorCode) {
        stopRecognizingAnimation();
        if (errorType != 0) {
            switch (errorType) {
                case SpeechRecognizer.ERROR_NO_MATCH:
                    break;
                case SpeechRecognizer.ERROR_AUDIO:
                    break;
                case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                    break;
                case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                    break;
                case SpeechRecognizer.ERROR_NETWORK:
                    break;
                case SpeechRecognizer.ERROR_CLIENT:
                    break;
                case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                    break;
                case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                    break;
                case SpeechRecognizer.ERROR_SERVER:
                    break;
                default:
                    break;
            }
            tvTips.setVisibility(View.GONE);
            sdkAnimationView.setVisibility(View.GONE);
            tvStopSpeech.setVisibility(View.GONE);
            clNoMatch.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPartialResults(String[] results) {
        if (results != null && results.length > 0) {
            String result = (String) results[0];
            if (!TextUtils.isEmpty(result)) {
                tvTips.setText(result.substring(0, result.length() - 1));
            }
        }
    }
}
